{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Main(
  main
) where

import Data.Function (($))
import Test.Tasty(defaultMain, testGroup)
import Prelude
import Test.Data.STL.Facet ( testFacet )
import Test.Data.STL.Facets ( testFacets )
import Test.Data.STL.Loop ( testLoop )
import Test.Data.STL.Number ( testNumber )
import Test.Data.STL.Solid ( testSolid )
import Test.Data.STL.STL ( testSTL )
import Test.Data.STL.ThreeOf ( testThreeOf )
import Test.Data.STL.Vertex ( testVertex )
import Test.Data.STL.Vertex3 ( testVertex3 )

main ::
  IO ()
main = defaultMain $
  testGroup "All STL Tests"
    [
      testFacet
    , testFacets
    , testLoop
    , testNumber
    , testSolid
    , testSTL
    , testThreeOf
    , testVertex
    , testVertex3
    ]
