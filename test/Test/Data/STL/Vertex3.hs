{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Vertex3(
  genVertex3
, genVertex3'
, printParseVertex3Test
, printParsePrintVertex3Test
, testVertex3
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.Vertex3
    ( Vertex3', Vertex3(..), parsecPrintParseVertex3' )
import Hedgehog ( (===), forAll, property, MonadGen )
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Data.STL.Vertex ( genVertex )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genVertex3 ::
  MonadGen g =>
  g a
  -> g n
  -> g (Vertex3 a n)
genVertex3 ga gn =
  Vertex3 <$>
    genVertex ga gn <*>
    genVertex ga gn <*>
    genVertex ga gn

genVertex3' ::
  MonadGen g =>
  g Vertex3'
genVertex3' =
  genVertex3 genIsoLatin1 genNumber

printParseVertex3Test ::
  TestTree
printParseVertex3Test =
  testProperty "parse . print Vertex3" (
    property $
      do  n <- forAll genVertex3'
          let s = review parsecPrintParseVertex3' n
          preview parsecPrintParseVertex3' s === Just n
  )

printParsePrintVertex3Test ::
  TestTree
printParsePrintVertex3Test =
  testProperty "print . parse . print Vertex3" (
    property $
      do  n <- forAll genVertex3'
          let s = review parsecPrintParseVertex3' n
          let t = preview parsecPrintParseVertex3' s
          (review parsecPrintParseVertex3' <$> t) === Just s
  )

testVertex3 ::
  TestTree
testVertex3 =
  testGroup "Vertex3"
    [
      printParseVertex3Test
    , printParsePrintVertex3Test
    ]
