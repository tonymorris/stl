{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Facets(
  genFacets
, genFacets'
, printParseFacetsTest
, printParsePrintFacetsTest
, testFacets
) where

import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.Facets
    ( Facets', Facets(..), parsecPrintParseFacets' )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Facet ( genFacet )
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genFacets ::
  MonadGen g =>
  g a
  -> g n
  -> g (Facets a n)
genFacets ga gn =
  Facets <$>
    Gen.nonEmpty (Range.linear 0 40) (genFacet ga gn)

genFacets' ::
  MonadGen g =>
  g Facets'
genFacets' =
  genFacets genIsoLatin1 genNumber

printParseFacetsTest ::
  TestTree
printParseFacetsTest =
  testProperty "parse . print Facets" (
    property $
      do  n <- forAll genFacets'
          let s = review parsecPrintParseFacets' n
          preview parsecPrintParseFacets' s === Just n
  )

printParsePrintFacetsTest ::
  TestTree
printParsePrintFacetsTest =
  testProperty "print . parse . print Facets" (
    property $
      do  n <- forAll genFacets'
          let s = review parsecPrintParseFacets' n
          let t = preview parsecPrintParseFacets' s
          (review parsecPrintParseFacets' <$> t) === Just s
  )

testFacets ::
  TestTree
testFacets =
  testGroup "Facets"
    [
      printParseFacetsTest
    , printParsePrintFacetsTest
    ]
