{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Facet(
  genFacet
, genFacet'
, printParseFacetTest
, printParsePrintFacetTest
, testFacet
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.Facet
    ( Facet', Facet(..), parsecPrintParseFacet' )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Loop ( genLoop )
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.ThreeOf ( genThreeOf )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genFacet ::
  MonadGen g =>
  g a
  -> g n
  -> g (Facet a n)
genFacet ga gn =
  Facet <$>
    Gen.nonEmpty (Range.linear 0 5) ga <*>
    Gen.nonEmpty (Range.linear 0 5) ga <*>
    genThreeOf ga gn <*>
    genLoop ga gn <*>
    Gen.nonEmpty (Range.linear 0 5) ga

genFacet' ::
  MonadGen g =>
  g Facet'
genFacet' =
  genFacet genIsoLatin1 genNumber

printParseFacetTest ::
  TestTree
printParseFacetTest =
  testProperty "parse . print Facet" (
    property $
      do  n <- forAll genFacet'
          let s = review parsecPrintParseFacet' n
          preview parsecPrintParseFacet' s === Just n
  )

printParsePrintFacetTest ::
  TestTree
printParsePrintFacetTest =
  testProperty "print . parse . print Facet" (
    property $
      do  n <- forAll genFacet'
          let s = review parsecPrintParseFacet' n
          let t = preview parsecPrintParseFacet' s
          (review parsecPrintParseFacet' <$> t) === Just s
  )

testFacet ::
  TestTree
testFacet =
  testGroup "Facet"
    [
      printParseFacetTest
    , printParsePrintFacetTest
    ]
