{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Util(
  genIsoLatin1
, genDecDigit
) where

import Data.Char.Space ( IsoLatin1(..) )
import Hedgehog ( MonadGen )
import Data.Digit ( DecDigit(..) )
import qualified Hedgehog.Gen as Gen

genIsoLatin1 ::
  MonadGen g =>
  g IsoLatin1
genIsoLatin1 =
  Gen.element [HorizontalTab_, LineFeed_, FormFeed_, CarriageReturn_, Whitespace_]

genDecDigit ::
  MonadGen g =>
  g DecDigit
genDecDigit =
  Gen.element [DecDigit0, DecDigit1, DecDigit2, DecDigit3, DecDigit4, DecDigit5, DecDigit6, DecDigit7, DecDigit8, DecDigit9]
