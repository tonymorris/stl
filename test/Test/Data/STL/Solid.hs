{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Solid(
  genSolid
, genSolid'
, printParseSolidTest
, printParsePrintSolidTest
, testSolid
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.Solid
    ( Solid', Solid(..), parsecPrintParseSolid' )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Facets ( genFacets )
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genSolid ::
  MonadGen g =>
  g a
  -> g n
  -> g (Solid a n)
genSolid ga gn =
  Solid <$>
    Gen.list (Range.linear 0 5) ga <*>
    Gen.nonEmpty (Range.linear 0 5) ga <*>
    Gen.nonEmpty (Range.linear 0 5) Gen.alphaNum <*>
    genFacets ga gn <*>
    Gen.nonEmpty (Range.linear 0 5) ga <*>
    Gen.nonEmpty (Range.linear 0 5) ga

genSolid' ::
  MonadGen g =>
  g Solid'
genSolid' =
  genSolid genIsoLatin1 genNumber

printParseSolidTest ::
  TestTree
printParseSolidTest =
  testProperty "parse . print Solid" (
    property $
      do  n <- forAll genSolid'
          let s = review parsecPrintParseSolid' n
          preview parsecPrintParseSolid' s === Just n
  )

printParsePrintSolidTest ::
  TestTree
printParsePrintSolidTest =
  testProperty "print . parse . print Solid" (
    property $
      do  n <- forAll genSolid'
          let s = review parsecPrintParseSolid' n
          let t = preview parsecPrintParseSolid' s
          (review parsecPrintParseSolid' <$> t) === Just s
  )

testSolid ::
  TestTree
testSolid =
  testGroup "Solid"
    [
      printParseSolidTest
    , printParsePrintSolidTest
    ]
