{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Loop(
  genLoop
, genLoop'
, printParseLoopTest
, printParsePrintLoopTest
, testLoop
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.Loop
    ( Loop', Loop(..), parsecPrintParseLoop' )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Data.STL.Vertex3 ( genVertex3 )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genLoop ::
  MonadGen g =>
  g a
  -> g n
  -> g (Loop a n)
genLoop ga gn =
  Loop <$>
    Gen.nonEmpty (Range.linear 0 5) ga <*>
    Gen.nonEmpty (Range.linear 0 5) ga <*>
    genVertex3 ga gn <*>
    Gen.nonEmpty (Range.linear 0 5) ga

genLoop' ::
  MonadGen g =>
  g Loop'
genLoop' =
  genLoop genIsoLatin1 genNumber

printParseLoopTest ::
  TestTree
printParseLoopTest =
  testProperty "parse . print Loop" (
    property $
      do  n <- forAll genLoop'
          let s = review parsecPrintParseLoop' n
          preview parsecPrintParseLoop' s === Just n
  )

printParsePrintLoopTest ::
  TestTree
printParsePrintLoopTest =
  testProperty "print . parse . print Loop" (
    property $
      do  n <- forAll genLoop'
          let s = review parsecPrintParseLoop' n
          let t = preview parsecPrintParseLoop' s
          (review parsecPrintParseLoop' <$> t) === Just s
  )

testLoop ::
  TestTree
testLoop =
  testGroup "Loop"
    [
      printParseLoopTest
    , printParsePrintLoopTest
    ]
