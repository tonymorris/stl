{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Vertex(
  genVertex
, genVertex'
, printParseVertexTest
, printParsePrintVertexTest
, testVertex
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.Vertex
    ( Vertex', Vertex(..), parsecPrintParseVertex' )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.ThreeOf ( genThreeOf )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genVertex ::
  MonadGen g =>
  g a
  -> g n
  -> g (Vertex a n)
genVertex ga gn =
  Vertex <$>
    Gen.nonEmpty (Range.linear 0 9) ga <*>
    genThreeOf ga gn

genVertex' ::
  MonadGen g =>
  g Vertex'
genVertex' =
  genVertex genIsoLatin1 genNumber

printParseVertexTest ::
  TestTree
printParseVertexTest =
  testProperty "parse . print Vertex" (
    property $
      do  n <- forAll genVertex'
          let s = review parsecPrintParseVertex' n
          preview parsecPrintParseVertex' s === Just n
  )

printParsePrintVertexTest ::
  TestTree
printParsePrintVertexTest =
  testProperty "print . parse . print Vertex" (
    property $
      do  n <- forAll genVertex'
          let s = review parsecPrintParseVertex' n
          let t = preview parsecPrintParseVertex' s
          (review parsecPrintParseVertex' <$> t) === Just s
  )

testVertex ::
  TestTree
testVertex =
  testGroup "Vertex"
    [
      printParseVertexTest
    , printParsePrintVertexTest
    ]
