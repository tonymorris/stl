{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.Number(
  genE
, genSign
, genNumber
, printParseNumberTest
, printParsePrintNumberTest
, testNumber
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.Number
    ( Number(..), E(..), Sign(..), parsecPrintParseNumber )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Util ( genDecDigit )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genE ::
  MonadGen g =>
  g E
genE =
  E <$>
    Gen.bool <*>
    genSign <*>
    Gen.nonEmpty (Range.linear 0 30) genDecDigit

genSign ::
  MonadGen g =>
  g Sign
genSign =
  Gen.element [Nosign, Plus, Minus]

genNumber ::
  MonadGen g =>
  g Number
genNumber =
  Number <$>
    genSign <*>
    Gen.nonEmpty (Range.linear 0 19) genDecDigit <*>
    Gen.list (Range.linear 0 9) genDecDigit <*>
    Gen.maybe genE

printParseNumberTest ::
  TestTree
printParseNumberTest =
  testProperty "parse . print Number" (
    property $
      do  n <- forAll genNumber
          let s = review parsecPrintParseNumber n
          preview parsecPrintParseNumber s === Just n
  )

printParsePrintNumberTest ::
  TestTree
printParsePrintNumberTest =
  testProperty "print . parse . print Number" (
    property $
      do  n <- forAll genNumber
          let s = review parsecPrintParseNumber n
          let t = preview parsecPrintParseNumber s
          (review parsecPrintParseNumber <$> t) === Just s
  )

testNumber ::
  TestTree
testNumber =
  testGroup "Number"
    [
      printParseNumberTest
    , printParsePrintNumberTest
    ]
