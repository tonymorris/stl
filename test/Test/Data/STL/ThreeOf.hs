{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.ThreeOf(
  genThreeOf
, genThreeOf'
, printParseThreeOfTest
, printParsePrintThreeOfTest
, testThreeOf
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.ThreeOf
    ( ThreeOf', ThreeOf(..), parsecPrintParseThreeOf' )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genThreeOf ::
  MonadGen g =>
  g a
  -> g n
  -> g (ThreeOf a n)
genThreeOf ga gn =
  ThreeOf <$>
    Gen.nonEmpty (Range.linear 0 9) ga <*>
    gn <*>
    Gen.nonEmpty (Range.linear 0 9) ga <*>
    gn <*>
    Gen.nonEmpty (Range.linear 0 9) ga <*>
    gn

genThreeOf' ::
  MonadGen g =>
  g ThreeOf'
genThreeOf' =
  genThreeOf genIsoLatin1 genNumber

printParseThreeOfTest ::
  TestTree
printParseThreeOfTest =
  testProperty "parse . print ThreeOf" (
    property $
      do  n <- forAll genThreeOf'
          let s = review parsecPrintParseThreeOf' n
          preview parsecPrintParseThreeOf' s === Just n
  )

printParsePrintThreeOfTest ::
  TestTree
printParsePrintThreeOfTest =
  testProperty "print . parse . print ThreeOf" (
    property $
      do  n <- forAll genThreeOf'
          let s = review parsecPrintParseThreeOf' n
          let t = preview parsecPrintParseThreeOf' s
          (review parsecPrintParseThreeOf' <$> t) === Just s
  )

testThreeOf ::
  TestTree
testThreeOf =
  testGroup "ThreeOf"
    [
      printParseThreeOfTest
    , printParsePrintThreeOfTest
    ]
