{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Test.Data.STL.STL(
  genSTL
, genSTL'
, printParseSTLTest
, printParsePrintSTLTest
, testSTL
) where

import Control.Applicative ( Applicative((<*>)) )
import Control.Lens ( preview, review )
import Data.Function ( ($) )
import Data.Functor( (<$>) )
import Data.Maybe ( Maybe(Just) )
import Data.STL.STL
    ( STL', STL(..), parsecPrintParseSTL' )
import Hedgehog ( (===), forAll, property, MonadGen )
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range
import Test.Data.STL.Number ( genNumber )
import Test.Data.STL.Solid ( genSolid )
import Test.Data.STL.Util ( genIsoLatin1 )
import Test.Tasty ( testGroup, TestTree )
import Test.Tasty.Hedgehog(testProperty)

genSTL ::
  MonadGen g =>
  g a
  -> g n
  -> g (STL a n)
genSTL ga gn =
  STL <$>
    genSolid ga gn <*>
    Gen.list (Range.linear 0 5) ga

genSTL' ::
  MonadGen g =>
  g STL'
genSTL' =
  genSTL genIsoLatin1 genNumber

printParseSTLTest ::
  TestTree
printParseSTLTest =
  testProperty "parse . print STL" (
    property $
      do  n <- forAll genSTL'
          let s = review parsecPrintParseSTL' n
          preview parsecPrintParseSTL' s === Just n
  )

printParsePrintSTLTest ::
  TestTree
printParsePrintSTLTest =
  testProperty "print . parse . print STL" (
    property $
      do  n <- forAll genSTL'
          let s = review parsecPrintParseSTL' n
          let t = preview parsecPrintParseSTL' s
          (review parsecPrintParseSTL' <$> t) === Just s
  )

testSTL ::
  TestTree
testSTL =
  testGroup "STL"
    [
      printParseSTLTest
    , printParsePrintSTLTest
    ]
