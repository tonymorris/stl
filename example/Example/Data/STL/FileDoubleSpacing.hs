{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Example.Data.STL.FileDoubleSpacing(
  fileDoubleSpacing
) where

import Control.Lens ( over )
import Control.Monad ( join )
import Data.Either ( Either(Right, Left) )
import Data.Semigroup ( Semigroup((<>)) )
import Data.STL ( stlSpaces, parsecReadSTLFile', writeSTLFile' )
import System.IO ( IO, FilePath, stderr, hPrint )

fileDoubleSpacing ::
  FilePath -- input file
  -> FilePath -- output file
  -> IO ()
fileDoubleSpacing i o =
  do  c <- parsecReadSTLFile' i
      case c of
        Left err ->
          hPrint stderr err
        Right s ->
          writeSTLFile'
            o
            (over stlSpaces (join (<>)) s)
