{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Example.Data.STL.FileRotateVertices(
  fileRotateVertices
) where

import Control.Category ( Category((.)) )
import Control.Lens ( over )
import Data.Either ( Either(Right, Left) )
import Data.STL
    ( HasVertex3(vertex3),
      Vertex3(Vertex3),
      stlLoopEach,
      parsecReadSTLFile',
      writeSTLFile' )
import System.IO ( IO, FilePath, stderr, hPrint )

fileRotateVertices ::
  FilePath -- input file
  -> FilePath -- output file
  -> IO ()
fileRotateVertices i o =
  do  c <- parsecReadSTLFile' i
      case c of
        Left err ->
          hPrint stderr err
        Right s ->
          writeSTLFile'
            o
            (over (stlLoopEach . vertex3) (\(Vertex3 x y z) -> Vertex3 y z x) s)
