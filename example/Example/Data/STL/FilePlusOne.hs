{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Example.Data.STL.FilePlusOne(
  filePlusOne
) where

import Control.Category ( Category((.)) )
import Control.Lens ( over )
import Data.Either ( Either(Right, Left) )
import Data.STL
    ( Printer,
      numericNumber,
      stlXYZ,
      parsecReadSTLFile',
      writeSTLFile,
      printIsoLatin1,
      printfPrinter )
import GHC.Float ( Double )
import GHC.Num ( Num((+)) )
import System.IO ( IO, FilePath, stderr, hPrint )

filePlusOne ::
  FilePath -- input file
  -> FilePath -- output file
  -> IO ()
filePlusOne i o =
  do  c <- parsecReadSTLFile' i
      case c of
        Left err ->
          hPrint stderr err
        Right s ->
          writeSTLFile
            printIsoLatin1
            (printfPrinter "%0.6f" :: Printer Double)
            o
            (over stlXYZ ((+1) . numericNumber) s)
