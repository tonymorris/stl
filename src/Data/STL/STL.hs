{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.STL(
  STL(..)
, stlFacetEach
, stlVertexEach
, stlThreeOfEach
, stlLoopEach
, stlSpaces
, stlXYZ
, stlThreeOfXYZ
, stlLoopXYZ
, HasSTL(..)
, AsSTL(..)
, parseSTL
, STL'
, parseSTL'
, printSTL
, printSTL'
, printParseSTL
, printParseSTL'
, parsecPrintParseSTL
, parsecPrintParseSTL'
) where

import Control.Applicative
    ( Applicative((<*>), pure), Alternative(many) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism', Lens', preview, _Right, prism', Traversal1, Traversal1' )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap, toList) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Apply ( Apply((<.>)) )
import Data.Functor.Contravariant.Divisible
    ( Divisible(divide) )
import Data.List.NonEmpty(NonEmpty((:|)))
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.Facet ( Facet, facetVertexEach )
import Data.STL.Loop ( HasLoop(loop), Loop )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer
    ( Printer, printer, listPrinter, printIsoLatin1 )
import Data.STL.Solid
    ( HasSolid(solid),
      Solid,
      solidFacetEach,
      solidSpaces,
      solidXYZ,
      parseSolid,
      printSolid,
      solidThreeOfXYZ,
      solidLoopXYZ )
import Data.STL.ThreeOf ( HasThreeOf(threeOf), ThreeOf )
import Data.STL.Vertex ( Vertex )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing )

data STL a n =
  STL
    (Solid a n)
    [a] -- trailing space
  deriving (Eq, Ord, Show, Generic)

stlFacetEach ::
  Traversal1 (STL a n) (STL a n') (Facet a n) (Facet a n')
stlFacetEach =
  let solid' f (STL s t) =
        fmap (\s' -> STL s' t) (f s)
  in  solid' . solidFacetEach

stlVertexEach ::
  Traversal1' (STL a n) (Vertex a n)
stlVertexEach =
  stlFacetEach . facetVertexEach

stlThreeOfEach ::
  Traversal1' (STL a n) (ThreeOf a n)
stlThreeOfEach =
  stlFacetEach . threeOf

stlLoopEach ::
  Traversal1' (STL a n) (Loop a n)
stlLoopEach =
  stlFacetEach . loop

stlSpaces ::
  Traversal1 (STL a n) (STL b n) (NonEmpty a) (NonEmpty b)
stlSpaces f (STL s []) =
  (\s' -> STL s' []) <$> solidSpaces f s
stlSpaces f (STL s (ht:tt)) =
  (\s' t' -> STL s' (toList t')) <$> solidSpaces f s <.> f (ht:|tt)

stlXYZ ::
  Traversal1 (STL a n) (STL a n') n n'
stlXYZ f (STL s t) =
  (\s' -> STL s' t) <$> solidXYZ f s

stlThreeOfXYZ ::
  Traversal1' (STL a n) n
stlThreeOfXYZ =
  solid . solidThreeOfXYZ

stlLoopXYZ ::
  Traversal1' (STL a n) n
stlLoopXYZ =
  solid . solidLoopXYZ

instance Bifunctor STL where
  bimap f g (STL s t) =
    STL (bimap f g s) (fmap f t)

instance Functor (STL a) where
  fmap =
    bimap id

instance Bifoldable STL where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 STL where
  bifoldMap1 f g (STL s []) =
    bifoldMap1 f g s
  bifoldMap1 f g (STL s (th:tt)) =
    bifoldMap1 f g s <> foldMap1 f (th:|tt)

instance Foldable (STL a) where
  foldMap =
    foldMap1

instance Foldable1 (STL a) where
  foldMap1 f (STL s1 _) =
    foldMap1 f s1

instance Bitraversable STL where
  bitraverse f g (STL s t) =
    STL <$>
      bitraverse f g s <*>
      traverse f t

instance Bitraversable1 STL where
  bitraverse1 f g (STL s []) =
    (\s' -> STL s' []) <$>
      bitraverse1 f g s
  bitraverse1 f g (STL s (th:tt)) =
    (\s' t' -> STL s' (toList t')) <$>
      bitraverse1 f g s <.>
      traverse1 f (th:|tt)

instance Traversable (STL a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (STL a) where
  traverse1 f (STL s t) =
    (\s' -> STL s' t) <$>
      traverse1 f s

class AsSTL c a n | c -> a n where
  _STL :: Prism' c (STL a n)

instance AsSTL (STL a n) a n where
  _STL = id

class HasSTL c a n | c -> a n where
  stl :: Lens' c (STL a n)
  {-# INLINE stlTrailingSpace #-}
  stlTrailingSpace :: Lens' c [a]
  stlTrailingSpace = stl . stlTrailingSpace

instance HasSTL (STL a n) a n where
  stl =
    id
  {-# INLINE stlTrailingSpace #-}
  stlTrailingSpace f (STL s t) =
    fmap (\t' -> STL s t') (f t)

instance HasSolid (STL a n) a n where
  solid f (STL s t) =
    fmap (\s' -> STL s' t) (f s)

-- | Parse a complete STL including trailing space.
parseSTL ::
  (Monad p, CharParsing p) =>
  p a
  -> p n
  -> p (STL a n)
parseSTL sp np =
  STL <$>
    parseSolid sp np <*>
    many sp

type STL' =
  STL IsoLatin1 Number

-- | Parse a complete STL including trailing space.
--
-- >>> Text.Parsec.parse (parseSTL' <* Text.Parser.Combinators.eof) "solid" "solid abc facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet endsolid abc   "
-- Right (STL (Solid [] (Whitespace_ :| []) ('a' :| "bc") (Facets (Facet (Whitespace_ :| []) (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing)) (Loop (Whitespace_ :| []) (Whitespace_ :| []) (Vertex3 (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing)))) (Whitespace_ :| [])) (Whitespace_ :| []) :| [])) (Whitespace_ :| []) (Whitespace_ :| [])) [Whitespace_,Whitespace_,Whitespace_])
parseSTL' ::
  (Monad p, CharParsing p) =>
  p STL'
parseSTL' =
  parseSTL parseIsoLatin1 parseNumber

-- | Parse a complete STL including trailing space.
printSTL ::
  Printer a
  -> Printer n
  -> Printer (STL a n)
printSTL pa pn =
  divide
    (\(STL s t) -> (s, t))
    (printSolid pa pn)
    (listPrinter pa)

-- | Parse a complete STL including trailing space.
--
-- >>> printer printSTL' (STL (Data.STL.Solid.Solid [] (Data.Char.Space.Whitespace_ :| []) ('a' :| "bc") (Data.STL.Facets.Facets (Data.STL.Facet.Facet (Data.Char.Space.Whitespace_ :| []) (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 :| []) [] Nothing)) (Data.STL.Loop.Loop (Data.Char.Space.Whitespace_ :| []) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Vertex3.Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing)))) (Data.Char.Space.Whitespace_ :| [])) (Data.Char.Space.Whitespace_ :| []) :| [])) (Data.Char.Space.Whitespace_ :| []) (Data.Char.Space.Whitespace_ :| [])) [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_])
-- "solid abc facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet endsolid abc   "
printSTL' ::
  Printer STL'
printSTL' =
  printSTL printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseSTL ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (STL a n) -> String -> Maybe (STL a n))
  -> Prism' String (STL a n)
printParseSTL ra rn pa pn parser =
  prism'
    (printer (printSTL ra rn))
    (parser (parseSTL pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseSTL' ::
  (CharParsing p, Monad p) =>
  (p STL' -> String -> Maybe STL')
  -> Prism' String STL'
printParseSTL' =
  printParseSTL
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseSTL' ::
  Prism' String STL'
parsecPrintParseSTL' =
  printParseSTL' (\p s -> preview _Right (Parsec.parse p "STL" s))

parsecPrintParseSTL ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (STL a n)
parsecPrintParseSTL ra rn pa pn =
  printParseSTL ra rn pa pn (\p s -> preview _Right (Parsec.parse p "STL" s))
