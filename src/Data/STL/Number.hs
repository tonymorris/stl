{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.Number(
  Sign(..)
, HasSign(..)
, AsSign(..)
, parseSign
, printSign
, E(..)
, HasE(..)
, AsE(..)
, parseE
, printE
, Number(..)
, HasNumber(..)
, AsNumber(..)
, parseNumber
, printNumber
, numericNumber
, printParseNumber
, parsecPrintParseNumber
) where

import Control.Category ( Category(id, (.)) )
import Control.Applicative
    ( (<$),
      Applicative((*>), (<*>), pure),
      optional,
      (<$>),
      Alternative((<|>)) )
import Control.Lens ( preview, view, prism', (#), Lens', Prism', _Right )
import Control.Monad ( Monad, Functor(fmap) )
import Data.Bool ( Bool(..), bool )
import Data.Digit
    ( DecDigit, parseDecimal, decDigitsIntegral, integralDecimal )
import Data.Either ( Either(Right) )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(toList, sum, null) )
import Data.Functor.Contravariant.Divisible
    ( Divisible(divide), divided )
import Data.List ( zipWith )
import Data.List.NonEmpty ( NonEmpty(..), some1 )
import Data.STL.Printer
    ( Printer(..), printer, listPrinter, modify, printDecDigits )
import Data.Maybe ( Maybe(..), maybe )
import Data.Ord ( Ord )
import Data.String ( String )
import GHC.Float ( Floating((**)) )
import GHC.Generics ( Generic )
import GHC.Num ( Num((+), fromInteger, (*), negate) )
import GHC.Real ( fromIntegral )
import GHC.Show ( Show )
import Prelude(Integer)
import qualified Text.Parsec as Parsec(parse)
import Text.Parser.Char ( CharParsing(char) )
import Text.Parser.Combinators ( Parsing(try) )

data Sign =
  Minus
  | Plus
  | Nosign
  deriving (Eq, Ord, Show, Generic)

class HasSign a where
  sign :: Lens' a Sign

instance HasSign Sign where
  sign =
    id

class AsSign a where
  _Sign ::
    Prism' a Sign
  _Minus ::
    Prism' a ()
  _Minus =
    _Sign . _Minus
  _Plus ::
    Prism' a ()
  _Plus =
    _Sign . _Plus
  _Nosign ::
    Prism' a ()
  _Nosign =
    _Sign . _Nosign

instance AsSign Sign where
  _Sign =
    id
  _Minus =
    prism'
      (\() -> Minus)
      (\case
        Minus ->
          Just ()
        _ ->
          Nothing
      )
  _Plus =
    prism'
      (\() -> Plus)
      (\case
        Plus ->
          Just ()
        _ ->
          Nothing
      )
  _Nosign =
    prism'
      (\() -> Nosign)
      (\case
        Nosign ->
          Just ()
        _ ->
          Nothing
      )

-- | Parse the optional sign of a number for STL
--
-- >>> Text.Parsec.parse (parseSign <* Text.Parser.Combinators.eof) "sign" "-"
-- Right Minus
-- >>> Text.Parsec.parse (parseSign <* Text.Parser.Combinators.eof) "sign" "+"
-- Right Plus
-- >>> Text.Parsec.parse (parseSign <* Text.Parser.Combinators.eof) "sign" ""
-- Right Nosign
-- >>> Text.Parsec.parse parseSign "sign" "x"
-- Right Nosign
parseSign ::
  CharParsing p =>
  p Sign
parseSign =
  Plus <$ try (char '+') <|>
  Minus <$ try (char '-') <|>
  pure Nosign

-- | Print the sign component of a number
--
-- >>> printer printSign Minus
-- "-"
-- >>> printer printSign Plus
-- "+"
-- >>> printer printSign Nosign
-- ""
printSign ::
  Printer Sign
printSign =
  Printer (\case
    Minus ->
      "-"
    Plus ->
      "+"
    Nosign ->
      ""
  )

data E =
  E
    Bool -- capitalised?
    Sign
    (NonEmpty DecDigit)
  deriving (Eq, Ord, Show, Generic)

class HasE a where
  e ::
    Lens' a E
  isCapitalised ::
    Lens' a Bool
  isCapitalised =
    e . isCapitalised
  eDigits ::
    Lens' a (NonEmpty DecDigit)
  eDigits =
    e . eDigits

instance HasE E where
  e =
    id
  isCapitalised f (E c s d) =
    fmap (\c' -> E c' s d) (f c)
  eDigits f (E c s d) =
    fmap (\d' -> E c s d') (f d)

instance HasSign E where
  sign f (E c s d) =
    fmap (\s' -> E c s' d) (f s)

class AsE a where
  _E ::
    Prism' a E

instance AsE E where
  _E =
    id

-- | Parse the scientific component of a number for STL
--
-- >>> Text.Parsec.parse (parseE <* Text.Parser.Combinators.eof) "E" "E-100"
-- Right (E True Minus (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse (parseE <* Text.Parser.Combinators.eof) "E" "E100"
-- Right (E True Nosign (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse (parseE <* Text.Parser.Combinators.eof) "E" "E+100"
-- Right (E True Plus (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse (parseE <* Text.Parser.Combinators.eof) "E" "e-100"
-- Right (E False Minus (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse (parseE <* Text.Parser.Combinators.eof) "E" "e100"
-- Right (E False Nosign (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse (parseE <* Text.Parser.Combinators.eof) "E" "e+100"
-- Right (E False Plus (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse parseE "E" "e+100e"
-- Right (E False Plus (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse parseE "E" "e+100+"
-- Right (E False Plus (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse parseE "E" "e+100-"
-- Right (E False Plus (DecDigit1 :| [DecDigit0,DecDigit0]))
-- >>> Text.Parsec.parse parseE "E" "e+100."
-- Right (E False Plus (DecDigit1 :| [DecDigit0,DecDigit0]))
parseE::
  (Monad p, CharParsing p) =>
  p E
parseE =
  E <$>
    (try (False <$ char 'e') <|> (True <$ char 'E')) <*>
    parseSign <*>
    some1 parseDecimal

-- | Print the scientific component of a number
--
-- >>> printer printE (E True Plus (Data.Digit.DecDigit4 :| [Data.Digit.DecDigit5, Data.Digit.DecDigit6]))
-- "E+456"
-- >>> printer printE (E False Minus (Data.Digit.DecDigit4 :| [Data.Digit.DecDigit5, Data.Digit.DecDigit6]))
-- "e-456"
-- >>> printer printE (E False Minus (Data.Digit.DecDigit4 :| []))
-- "e-4"
printE ::
  Printer E
printE =
  divide
    (\(E c s d) -> ((c, s), d))
    (divided (Printer (bool "e" "E")) printSign) printDecDigits

data Number =
  Number
    Sign
    (NonEmpty DecDigit) -- first digits
    [DecDigit] -- optional digits following decimal
    (Maybe E)
  deriving (Eq, Ord, Show, Generic)

class HasNumber a where
  number ::
    Lens' a Number
  numberFirstDigits ::
    Lens' a (NonEmpty DecDigit)
  numberFirstDigits =
    number . numberFirstDigits
  numberFollowingDigits ::
    Lens' a [DecDigit]
  numberFollowingDigits =
    number . numberFollowingDigits
  numberE ::
    Lens' a (Maybe E)
  numberE =
    number . numberE

instance HasNumber Number where
  number =
    id
  numberFirstDigits f (Number s d1 d2 ee) =
    fmap (\d1' -> Number s d1' d2 ee) (f d1)
  numberFollowingDigits f (Number s d1 d2 ee) =
    fmap (\d2' -> Number s d1 d2' ee) (f d2)
  numberE f (Number s d1 d2 ee) =
    fmap (\ee' -> Number s d1 d2 ee') (f ee)

instance HasSign Number where
  sign f (Number s d1 d2 ee) =
    fmap (\s' -> Number s' d1 d2 ee) (f s)

class AsNumber a where
  _Number ::
    Prism' a Number

instance AsNumber Number where
  _Number =
    id

-- | Parse a floating-point number for STL
--
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "100"
-- Right (Number Nosign (DecDigit1 :| [DecDigit0,DecDigit0]) [] Nothing)
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "100.0"
-- Right (Number Nosign (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] Nothing)
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] Nothing)
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "+100.0"
-- Right (Number Plus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] Nothing)
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] Nothing)
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0e+5"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E False Plus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0e5"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E False Nosign (DecDigit5 :| []))))
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0e-5"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E False Nosign (DecDigit5 :| []))))
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0E+5"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Plus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0E5"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Nosign (DecDigit5 :| []))))
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "-100.0E-5"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Minus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse (parseNumber <* Text.Parser.Combinators.eof) "number" "+100.0E-5"
-- Right (Number Plus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Minus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse parseNumber "number" "-100.0E-5e"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Minus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse parseNumber "number" "-100.0E-5+"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Minus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse parseNumber "number" "-100.0E-5-"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Minus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse parseNumber "number" "-100.0E-5."
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] (Just (E True Minus (DecDigit5 :| []))))
-- >>> Text.Parsec.parse parseNumber "number" "-100.0-"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] Nothing)
-- >>> Text.Parsec.parse parseNumber "number" "-100.0+"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] Nothing)
-- >>> Text.Parsec.parse parseNumber "number" "-100.0."
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [DecDigit0] Nothing)
-- >>> Text.Parsec.parse parseNumber "number" "-100-"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [] Nothing)
-- >>> Text.Parsec.parse parseNumber "number" "-100+"
-- Right (Number Minus (DecDigit1 :| [DecDigit0,DecDigit0]) [] Nothing)
parseNumber::
  (Monad p, CharParsing p) =>
  p Number
parseNumber =
  Number <$>
    parseSign <*>
    some1 parseDecimal <*>
    (maybe [] (\(h:|t) -> h:t) <$> optional (char '.' *> some1 parseDecimal)) <*>
    optional parseE

-- | Print a number to a string
--
-- >>> printer printNumber (Number Nosign (Data.Digit.DecDigit1 :| [Data.Digit.DecDigit0,Data.Digit.DecDigit0]) [] Nothing)
-- "100"
-- >>> printer printNumber (Number Nosign (Data.Digit.DecDigit1 :| [Data.Digit.DecDigit0,Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing)
-- "100.0"
-- >>> printer printNumber (Number Plus (Data.Digit.DecDigit1 :| [Data.Digit.DecDigit0,Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing)
-- "+100.0"
-- >>> printer printNumber (Number Plus (Data.Digit.DecDigit1 :| [Data.Digit.DecDigit0,Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] (Just (E True Plus (Data.Digit.DecDigit4 :| [Data.Digit.DecDigit5, Data.Digit.DecDigit6]))))
-- "+100.0E+456"
-- >>> printer printNumber (Number Minus (Data.Digit.DecDigit1 :| [Data.Digit.DecDigit0,Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] (Just (E False Nosign (Data.Digit.DecDigit4 :| [Data.Digit.DecDigit5, Data.Digit.DecDigit6]))))
-- "-100.0e456"
-- >>> printer printNumber (Number Plus (Data.Digit.DecDigit1 :| [Data.Digit.DecDigit0,Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] (Just (E False Minus (Data.Digit.DecDigit4 :| [Data.Digit.DecDigit5, Data.Digit.DecDigit6]))))
-- "+100.0e-456"
-- >>> printer printNumber (Number Plus (Data.Digit.DecDigit1 :| []) [Data.Digit.DecDigit0] (Just (E False Minus (Data.Digit.DecDigit4 :| [Data.Digit.DecDigit5, Data.Digit.DecDigit6]))))
-- "+1.0e-456"
-- >>> printer printNumber (Number Plus (Data.Digit.DecDigit1 :| []) [] (Just (E False Minus (Data.Digit.DecDigit4 :| [Data.Digit.DecDigit5, Data.Digit.DecDigit6]))))
-- "+1e-456"
printNumber ::
  Printer Number
printNumber =
  let printDec =
        modify (\s -> bool ('.':s) s (null s)) printDecDigits
  in  divide (\(Number s d1 d2 ee) -> (((s, d1), d2), ee)) (divide id (divided printSign printDecDigits) printDec) (listPrinter printE)

-- | Given a parse function, provide the prism to print and parse.
printParseNumber ::
  (CharParsing p, Monad p) =>
  (p Number -> String -> Maybe Number)
  -> Prism' String Number
printParseNumber parser =
  prism'
    (printer printNumber)
    (parser parseNumber)

parsecPrintParseNumber ::
  Prism' String Number
parsecPrintParseNumber =
  printParseNumber (\p s -> preview _Right (Parsec.parse p "Number" s))

-- |
--
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| []) [] Nothing)
-- 1.0
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| []) [] Nothing)
-- -1.0
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x0, Data.Digit.x0]) [] Nothing)
-- 100.0
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x0, Data.Digit.x0]) [] Nothing)
-- -100.0
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x0, Data.Digit.x0]) [Data.Digit.x3, Data.Digit.x4, Data.Digit.x5] Nothing)
-- 100.345
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x0, Data.Digit.x0]) [Data.Digit.x0, Data.Digit.x0, Data.Digit.x3, Data.Digit.x4, Data.Digit.x5] Nothing)
-- 100.00345
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x0, Data.Digit.x0]) [Data.Digit.x3, Data.Digit.x4, Data.Digit.x5] Nothing)
-- -100.345
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x0, Data.Digit.x0]) [Data.Digit.x0, Data.Digit.x0, Data.Digit.x3, Data.Digit.x4, Data.Digit.x5] Nothing)
-- -100.00345
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E True Minus (Data.Digit.x2 :| []))))
-- 1.23
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E True Minus (Data.Digit.x2 :| []))))
-- -1.23
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E False Minus (Data.Digit.x2 :| []))))
-- 1.23
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E False Minus (Data.Digit.x2 :| []))))
-- -1.23
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E True Plus (Data.Digit.x2 :| []))))
-- 12300.0
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E True Plus (Data.Digit.x2 :| []))))
-- -12300.0
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E False Plus (Data.Digit.x2 :| []))))
-- 12300.0
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [] (Just (E False Plus (Data.Digit.x2 :| []))))
-- -12300.0
-- >>> numericNumber (Number Plus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [Data.Digit.x0, Data.Digit.x0, Data.Digit.x3, Data.Digit.x4, Data.Digit.x5] (Just (E True Plus (Data.Digit.x2 :| []))))
-- 12300.345
-- >>> numericNumber (Number Minus (Data.Digit.x1 :| [Data.Digit.x2, Data.Digit.x3]) [Data.Digit.x0, Data.Digit.x0, Data.Digit.x3, Data.Digit.x4, Data.Digit.x5] (Just (E True Plus (Data.Digit.x2 :| []))))
-- -12300.345
numericNumber ::
  (HasSign s, HasNumber s, Floating a) =>
  s
  -> a
numericNumber n =
  let signF Minus =
        negate
      signF Plus =
        id
      signF Nosign =
        id
      val =
        decDigitsIntegral (Right (view numberFirstDigits n))
      expon Nothing x =
        x
      expon (Just (E _ s d)) x =
        x * 10 ** signF s (fromIntegral (decDigitsIntegral (Right d) :: Integer))
      decimalComponent =
        sum (zipWith (\b a -> fromInteger (integralDecimal # a) * 10 ** fromInteger b) [(-1), (-2)..] (toList (view numberFollowingDigits n)))
  in  signF (view sign n) (expon (view numberE n) (fromInteger val + decimalComponent))
