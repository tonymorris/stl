{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.Facets(
  Facets(..)
, facetEach
, facetsVertexEach
, threeOfEach
, loopEach
, facetsSpaces
, facetsXYZ
, facetsThreeOfXYZ
, facetsLoopXYZ
, HasFacets(..)
, AsFacets(..)
, parseFacets
, Facets'
, parseFacets'
, printFacets
, printFacets'
, printParseFacets
, printParseFacets'
, parsecPrintParseFacets
, parsecPrintParseFacets'
) where

import Control.Applicative ( Applicative(pure) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism',
      Lens',
      Index,
      IxValue,
      Ixed(..),
      _Wrapped,
      Rewrapped,
      Wrapped(..),
      preview,
      iso,
      _Right,
      prism',
      over,
      Each(..),
      Plated(..),
      Traversal1,
      Traversal1' )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Contravariant ( Contravariant(contramap) )
import Data.Int ( Int )
import Data.List.NonEmpty ( NonEmpty(..), some1 )
import qualified Data.List.NonEmpty as NonEmpty( cons )
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.Facet
    ( Facet,
      facetVertexEach,
      facetSpaces,
      facetXYZ,
      parseFacet,
      printFacet,
      facetThreeOfXYZ,
      facetLoopXYZ )
import Data.STL.Loop ( HasLoop(loop), Loop )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer
    ( Printer, printer, listPrinter, printIsoLatin1 )
import Data.STL.ThreeOf ( HasThreeOf(threeOf), ThreeOf )
import Data.STL.Vertex ( Vertex )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing )
import Text.Parser.Combinators ( Parsing(try) )

newtype Facets a n =
  Facets
    (NonEmpty (Facet a n))
  deriving (Eq, Ord, Show, Generic)

instance Facets a n ~ t =>
  Rewrapped (Facets a' n') t

instance Wrapped (Facets a n) where
  type Unwrapped (Facets a n) =
    NonEmpty (Facet a n)
  _Wrapped' =
    iso (\(Facets x) -> x) Facets

facetEach ::
  Traversal1 (Facets a n) (Facets b n') (Facet a n) (Facet b n')
facetEach =
  _Wrapped . traverse1

facetsVertexEach ::
  Traversal1' (Facets a n) (Vertex a n)
facetsVertexEach =
  facetEach . facetVertexEach

threeOfEach ::
  Traversal1' (Facets a n) (ThreeOf a n)
threeOfEach =
  facetEach . threeOf

loopEach ::
  Traversal1' (Facets a n) (Loop a n)
loopEach =
  facetEach . loop

facetsSpaces ::
  Traversal1 (Facets a n) (Facets b n) (NonEmpty a) (NonEmpty b)
facetsSpaces =
  facetEach . facetSpaces

facetsXYZ ::
  Traversal1 (Facets a n) (Facets a n') n n'
facetsXYZ =
  facetEach . facetXYZ

facetsThreeOfXYZ ::
  Traversal1' (Facets a n) n
facetsThreeOfXYZ =
  facetEach . facetThreeOfXYZ

facetsLoopXYZ ::
  Traversal1' (Facets a n) n
facetsLoopXYZ =
  facetEach . facetLoopXYZ

{-

facetXYZ ::
  Traversal1 (Facet a n) (Facet a n') n n'
facetXYZ f (Facet s1 s2 d l s3) =
  (\d' l' -> Facet s1 s2 d' l' s3) <$> threeOfXYZ f d <.> loopXYZ f l

facetThreeOfXYZ ::
  Traversal1' (Facet a n) n
facetThreeOfXYZ f (Facet s1 s2 d l s3) =
  (\d' -> Facet s1 s2 d' l s3) <$> threeOfXYZ f d

facetLoopXYZ ::
  Traversal1' (Facet a n) n
facetLoopXYZ f (Facet s1 s2 d l s3) =
  (\l' -> Facet s1 s2 d l' s3) <$> loopXYZ f l
-}

instance Bifunctor Facets where
  bimap f g (Facets x) =
    Facets (fmap (bimap f g) x)

instance Functor (Facets a) where
  fmap =
    bimap id

instance Bifoldable Facets where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 Facets where
  bifoldMap1 f g (Facets x) =
    foldMap1 (bifoldMap1 f g) x

instance Foldable (Facets a) where
  foldMap =
    foldMap1

instance Foldable1 (Facets a) where
  foldMap1 f (Facets x) =
    foldMap1 (foldMap1 f) x

instance Bitraversable Facets where
  bitraverse f g (Facets x) =
    Facets <$>
      traverse (bitraverse f g) x

instance Bitraversable1 Facets where
  bitraverse1 f g (Facets x) =
    Facets <$>
      traverse1 (bitraverse1 f g) x

instance Traversable (Facets a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (Facets a) where
  traverse1 f (Facets x) =
    Facets <$>
      traverse1 (traverse1 f) x

class AsFacets c a n | c -> a n where
  _Facets :: Prism' c (Facets a n)

instance AsFacets (Facets a n) a n where
  _Facets = id

class HasFacets c a n | c -> a n where
  facets :: Lens' c (Facets a n)

instance HasFacets (Facets a n) a n where
  facets =
    id

instance Semigroup (Facets a n) where
  Facets x <> Facets y =
    Facets (x <> y)

instance Plated (Facets a n) where
  plate f (Facets (h :| t)) =
    case t of
      [] ->
        pure (Facets (h :| t))
      u:us ->
        over _Wrapped (NonEmpty.cons h) <$> f (Facets (u :| us))

instance Each (Facets a n) (Facets b n') (Facet a n) (Facet b n') where
  each =
    _Wrapped . each

type instance Index (Facets a n) = Int
type instance IxValue (Facets a n) = (Facet a n)

instance Ixed (Facets a n) where
  ix n =
    _Wrapped . ix n

-- | A non-empty list of facets from STL
parseFacets ::
  (CharParsing p, Monad p) =>
  p a
  -> p n
  -> p (Facets a n)
parseFacets sp np =
  Facets <$> some1 (try (parseFacet sp np))

type Facets' =
  Facets IsoLatin1 Number

-- | Parse a non-empty list of facets from STL
--
-- >>> Text.Parsec.parse (parseFacets' <* Text.Parser.Combinators.eof) "facet" " facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet"
-- Right (Facets (Facet (Whitespace_ :| []) (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing)) (Loop (Whitespace_ :| []) (Whitespace_ :| []) (Vertex3 (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing)))) (Whitespace_ :| [])) (Whitespace_ :| []) :| []))
parseFacets' ::
  (Monad p, CharParsing p) =>
  p Facets'
parseFacets' =
  parseFacets parseIsoLatin1 parseNumber

-- | Print a non-empty list of facets from STL
printFacets ::
  Printer a
  -> Printer n
  -> Printer (Facets a n)
printFacets pa pn =
  contramap
    (\(Facets x) -> x)
    (listPrinter (printFacet pa pn))

-- | Print a non-empty list of facets from STL
--
-- >>> printer printFacets' (Data.STL.Facets.Facets (Data.STL.Facet.Facet (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 Data.List.NonEmpty.:| []) [] Nothing)) (Data.STL.Loop.Loop (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Vertex3.Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing)))) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [])) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) Data.List.NonEmpty.:| []))
-- " facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet"
printFacets' ::
  Printer Facets'
printFacets' =
  printFacets printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseFacets ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (Facets a n) -> String -> Maybe (Facets a n))
  -> Prism' String (Facets a n)
printParseFacets ra rn pa pn parser =
  prism'
    (printer (printFacets ra rn))
    (parser (parseFacets pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseFacets' ::
  (CharParsing p, Monad p) =>
  (p Facets' -> String -> Maybe Facets')
  -> Prism' String Facets'
printParseFacets' =
  printParseFacets
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseFacets' ::
  Prism' String Facets'
parsecPrintParseFacets' =
  printParseFacets' (\p s -> preview _Right (Parsec.parse p "Facets" s))

parsecPrintParseFacets ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (Facets a n)
parsecPrintParseFacets ra rn pa pn =
  printParseFacets ra rn pa pn (\p s -> preview _Right (Parsec.parse p "Facets" s))
