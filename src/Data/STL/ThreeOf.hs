{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.ThreeOf(
  ThreeOf(..)
, threeOfSpaces
, threeOfValues
, threeOfXYZ
, HasThreeOf(..)
, AsThreeOf(..)
, parseThreeOf
, ThreeOf'
, parseThreeOf'
, printThreeOf
, printThreeOf'
, printParseThreeOf
, printParseThreeOf'
, parsecPrintParseThreeOf
, parsecPrintParseThreeOf'
) where

import Control.Applicative ( Applicative((<*>), pure) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism', Lens', preview, _Right, prism', Traversal1 )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Apply ( Apply((<.>)) )
import Data.Functor.Contravariant.Divisible
    ( Divisible(divide), divided )
import Data.List.NonEmpty(NonEmpty, some1)
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer
    ( Printer, printer, listPrinter, printIsoLatin1 )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing )

data ThreeOf a n =
  ThreeOf
    (NonEmpty a)
    n
    (NonEmpty a)
    n
    (NonEmpty a)
    n
  deriving (Eq, Ord, Show, Generic)

threeOfSpaces ::
  Traversal1 (ThreeOf a n) (ThreeOf b n) (NonEmpty a) (NonEmpty b)
threeOfSpaces f (ThreeOf s1 x s2 y s3 z) =
  (\s1' s2' s3' -> ThreeOf s1' x s2' y s3' z) <$> f s1 <.> f s2 <.> f s3

threeOfValues ::
  Traversal1 (ThreeOf a n) (ThreeOf a n') n n'
threeOfValues f (ThreeOf s1 x s2 y s3 z) =
  (\x' y' z' -> ThreeOf s1 x' s2 y' s3 z') <$> f x <.> f y <.> f z

threeOfXYZ ::
  Traversal1 (ThreeOf a n) (ThreeOf a n') n n'
threeOfXYZ f (ThreeOf s1 x s2 y s3 z) =
  (\x' y' z' -> ThreeOf s1 x' s2 y' s3 z') <$> f x <.> f y <.> f z

instance Bifunctor ThreeOf where
  bimap f g (ThreeOf s1 x s2 y s3 z) =
    ThreeOf (fmap f s1) (g x) (fmap f s2) (g y) (fmap f s3) (g z)

instance Functor (ThreeOf a) where
  fmap =
    bimap id

instance Bifoldable ThreeOf where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 ThreeOf where
  bifoldMap1 f g (ThreeOf s1 x s2 y s3 z) =
    foldMap1 f s1 <> g x <> foldMap1 f s2 <> g y <> foldMap1 f s3 <> g z

instance Foldable (ThreeOf a) where
  foldMap =
    foldMap1

instance Foldable1 (ThreeOf a) where
  foldMap1 f (ThreeOf _ x _ y _ z) =
    f x <> f y <> f z

instance Bitraversable ThreeOf where
  bitraverse f g (ThreeOf s1 x s2 y s3 z) =
    ThreeOf <$>
      traverse f s1 <*>
      g x <*>
      traverse f s2 <*>
      g y <*>
      traverse f s3 <*>
      g z

instance Bitraversable1 ThreeOf where
  bitraverse1 f g (ThreeOf s1 x s2 y s3 z) =
    ThreeOf <$>
      traverse1 f s1 <.>
      g x <.>
      traverse1 f s2 <.>
      g y <.>
      traverse1 f s3 <.>
      g z

instance Traversable (ThreeOf a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (ThreeOf a) where
  traverse1 f (ThreeOf s1 x s2 y s3 z) =
    (\x' y' z' -> ThreeOf s1 x' s2 y' s3 z') <$>
      f x <.>
      f y <.>
      f z

class AsThreeOf c a n | c -> a n where
  _ThreeOf ::
    Prism' c (ThreeOf a n)

instance AsThreeOf (ThreeOf a n) a n where
  _ThreeOf =
    id

class HasThreeOf c a n | c -> a n where
  threeOf ::
    Lens' c (ThreeOf a n)
  {-# INLINE threeOfSpaces1' #-}
  threeOfSpaces1' :: Lens' c (NonEmpty a)
  threeOfSpaces1' = threeOf . threeOfSpaces1'
  {-# INLINE threeOfX' #-}
  threeOfX' :: Lens' c n
  threeOfX' = threeOf . threeOfX'
  {-# INLINE threeOfSpaces2' #-}
  threeOfSpaces2' :: Lens' c (NonEmpty a)
  threeOfSpaces2' = threeOf . threeOfSpaces2'
  {-# INLINE threeOfY' #-}
  threeOfY' :: Lens' c n
  threeOfY' = threeOf . threeOfY'
  {-# INLINE threeOfSpaces3' #-}
  threeOfSpaces3' :: Lens' c (NonEmpty a)
  threeOfSpaces3' = threeOf . threeOfSpaces3'
  {-# INLINE threeOfZ' #-}
  threeOfZ' :: Lens' c n
  threeOfZ' = threeOf . threeOfZ'

instance HasThreeOf (ThreeOf a n) a n where
  threeOf =
    id
  {-# INLINE threeOfSpaces1' #-}
  threeOfSpaces1' f (ThreeOf s1 x s2 y s3 z) =
    fmap (\s1' -> ThreeOf s1' x s2 y s3 z) (f s1)
  {-# INLINE threeOfX' #-}
  threeOfX' f (ThreeOf s1 x s2 y s3 z) =
    fmap (\x' -> ThreeOf s1 x' s2 y s3 z) (f x)
  {-# INLINE threeOfSpaces2' #-}
  threeOfSpaces2' f (ThreeOf s1 x s2 y s3 z) =
    fmap (\s2' -> ThreeOf s1 x s2' y s3 z) (f s2)
  {-# INLINE threeOfY' #-}
  threeOfY' f (ThreeOf s1 x s2 y s3 z) =
    fmap (\y' -> ThreeOf s1 x s2 y' s3 z) (f y)
  {-# INLINE threeOfSpaces3' #-}
  threeOfSpaces3' f (ThreeOf s1 x s2 y s3 z) =
    fmap (\s3' -> ThreeOf s1 x s2 y s3' z) (f s3)
  {-# INLINE threeOfZ' #-}
  threeOfZ' f (ThreeOf s1 x s2 y s3 z) =
    fmap (\z' -> ThreeOf s1 x s2 y s3 z') (f z)

-- | Parse a three consequent numeric values of STL including leading space
parseThreeOf ::
  (Monad p, CharParsing p) =>
  p a
  -> p n
  -> p (ThreeOf a n)
parseThreeOf sp np =
  ThreeOf <$>
    some1 sp <*>
    np <*>
    some1 sp <*>
    np <*>
    some1 sp <*>
    np

type ThreeOf' =
  ThreeOf IsoLatin1 Number

-- | Parse a three consequent numeric values of STL including leading space
--
-- >>> Text.Parsec.parse (parseThreeOf' <* Text.Parser.Combinators.eof) "threeOf" " 10.0 92.3 5.1"
-- Right (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| [DecDigit0]) [DecDigit0] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [DecDigit1] Nothing))
-- >>> Text.Parsec.parse (parseThreeOf' <* Text.Parser.Combinators.eof) "threeOf" "  10.0 -92.3 5.1"
-- Right (ThreeOf (Whitespace_ :| [Whitespace_]) (Number Nosign (DecDigit1 :| [DecDigit0]) [DecDigit0] Nothing) (Whitespace_ :| []) (Number Minus (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [DecDigit1] Nothing))
-- >>> Text.Parsec.parse (parseThreeOf' <* Text.Parser.Combinators.eof) "threeOf" "\n  \n   \t 10.0 -92.3 1e+10"
-- Right (ThreeOf (LineFeed_ :| [Whitespace_,Whitespace_,LineFeed_,Whitespace_,Whitespace_,Whitespace_,HorizontalTab_,Whitespace_]) (Number Nosign (DecDigit1 :| [DecDigit0]) [DecDigit0] Nothing) (Whitespace_ :| []) (Number Minus (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] (Just (E False Plus (DecDigit1 :| [DecDigit0])))))
-- >>> Text.Parsec.parse (parseThreeOf') "threeOf" "  -5.6e+10 -92.3 1e+10    \n"
-- Right (ThreeOf (Whitespace_ :| [Whitespace_]) (Number Minus (DecDigit5 :| []) [DecDigit6] (Just (E False Plus (DecDigit1 :| [DecDigit0])))) (Whitespace_ :| []) (Number Minus (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] (Just (E False Plus (DecDigit1 :| [DecDigit0])))))
parseThreeOf' ::
  (Monad p, CharParsing p) =>
  p ThreeOf'
parseThreeOf' =
  parseThreeOf parseIsoLatin1 parseNumber

-- | Print a three consequent numeric value of STL including leading space
printThreeOf ::
  Printer a
  -> Printer n
  -> Printer (ThreeOf a n)
printThreeOf pa pn =
  divide
    (\(ThreeOf s1 x s2 y s3 z) -> (((((s1, x), s2), y), s3), z))
    (
      divided
        (
          divided
            (
              divided
                (
                  divided
                    (listPrinter pa)
                    pn
                )
                (listPrinter pa)
            )
            pn
        )
        (listPrinter pa)
    )
    pn

-- | Print a three consequent numeric value of STL including leading space
--
-- >>> printer printThreeOf' (ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [Data.Digit.DecDigit1] Nothing))
-- " 10.0 92.3 5.1"
-- >>> printer printThreeOf' (ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_]) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [Data.Digit.DecDigit1] Nothing))
-- "  10.0 -92.3 5.1"
-- >>> printer printThreeOf' (ThreeOf (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.HorizontalTab_,Data.Char.Space.Whitespace_]) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] (Just (Data.STL.Number.E False Data.STL.Number.Plus (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0])))))
-- "\n  \n   \t 10.0 -92.3 1e+10"
-- >>> printer printThreeOf' (ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_]) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [Data.Digit.DecDigit6] (Just (Data.STL.Number.E False Data.STL.Number.Plus (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0])))) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] (Just (Data.STL.Number.E False Data.STL.Number.Plus (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0])))))
-- "  -5.6e+10 -92.3 1e+10"
printThreeOf' ::
  Printer ThreeOf'
printThreeOf' =
  printThreeOf printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseThreeOf ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (ThreeOf a n) -> String -> Maybe (ThreeOf a n))
  -> Prism' String (ThreeOf a n)
printParseThreeOf ra rn pa pn parser =
  prism'
    (printer (printThreeOf ra rn))
    (parser (parseThreeOf pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseThreeOf' ::
  (CharParsing p, Monad p) =>
  (p ThreeOf' -> String -> Maybe ThreeOf')
  -> Prism' String ThreeOf'
printParseThreeOf' =
  printParseThreeOf
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseThreeOf' ::
  Prism' String ThreeOf'
parsecPrintParseThreeOf' =
  printParseThreeOf' (\p s -> preview _Right (Parsec.parse p "ThreeOf" s))

parsecPrintParseThreeOf ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (ThreeOf a n)
parsecPrintParseThreeOf ra rn pa pn =
  printParseThreeOf ra rn pa pn (\p s -> preview _Right (Parsec.parse p "ThreeOf" s))

