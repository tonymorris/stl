{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.Solid(
  Solid(..)
, solidFacetEach
, solidVertexEach
, solidThreeOfEach
, solidLoopEach
, solidSpaces
, solidXYZ
, solidThreeOfXYZ
, solidLoopXYZ
, HasSolid(..)
, AsSolid(..)
, parseSolid
, Solid'
, parseSolid'
, printSolid
, printSolid'
, printParseSolid
, printParseSolid'
, parsecPrintParseSolid
, parsecPrintParseSolid'
) where

import Control.Applicative
    ( Applicative(pure, (<*>)), Alternative(many) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism',
      Lens',
      _Wrapped,
      preview,
      _Right,
      prism',
      Traversal1,
      Traversal1' )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Bool ( not )
import Data.Char ( Char, isSpace )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap, toList) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Apply ( Apply((<.>)) )
import Data.Functor.Contravariant.Divisible
    ( Divisible(divide), divided )
import Data.List.NonEmpty(NonEmpty((:|)), some1)
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.Facet ( Facet, facetVertexEach )
import Data.STL.Facets
    ( HasFacets(..),
      Facets,
      facetsSpaces,
      facetsXYZ,
      parseFacets,
      printFacets,
      facetsLoopXYZ,
      facetsThreeOfXYZ )
import Data.STL.Loop ( HasLoop(loop), Loop )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer
    ( Printer,
      printer,
      listPrinter,
      stringPrinter,
      str,
      printIsoLatin1 )
import Data.STL.ThreeOf ( HasThreeOf(threeOf), ThreeOf )
import Data.STL.Vertex ( Vertex )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing(satisfy, string) )

data Solid a n =
  Solid
    [a] -- leading spaces before "solid"
    (NonEmpty a) -- spaces after "solid"
    (NonEmpty Char) -- name (contains any char except '\n')
    (Facets a n)
    (NonEmpty a) -- leading spaces before "endsolid"
    (NonEmpty a) -- spaces after "endsolid"
  deriving (Eq, Ord, Show, Generic)

solidFacetEach ::
  Traversal1 (Solid a n) (Solid a n') (Facet a n) (Facet a n')
solidFacetEach =
  let facets' = \f (Solid s1 s2 n t s3 s4) -> fmap (\t' -> Solid s1 s2 n t' s3 s4) (f t)
  in  facets' . _Wrapped . traverse1

solidVertexEach ::
  Traversal1' (Solid a n) (Vertex a n)
solidVertexEach =
  solidFacetEach . facetVertexEach

solidThreeOfEach ::
  Traversal1' (Solid a n) (ThreeOf a n)
solidThreeOfEach =
  solidFacetEach . threeOf

solidLoopEach ::
  Traversal1' (Solid a n) (Loop a n)
solidLoopEach =
  solidFacetEach . loop

solidSpaces ::
  Traversal1 (Solid a n) (Solid b n) (NonEmpty a) (NonEmpty b)
solidSpaces f (Solid [] s2 n t s3 s4) =
  (\s2' t' s3' s4' -> Solid [] s2' n t' s3' s4') <$> f s2 <.> facetsSpaces f t <.> f s3 <.> f s4
solidSpaces f (Solid (s1h:s1t) s2 n t s3 s4) =
  (\s1' s2' t' s3' s4' -> Solid (toList s1') s2' n t' s3' s4') <$> f (s1h:|s1t) <.> f s2 <.> facetsSpaces f t <.> f s3 <.> f s4

solidXYZ ::
  Traversal1 (Solid a n) (Solid a n') n n'
solidXYZ f (Solid s1 s2 n t s3 s4) =
  (\t' -> Solid s1 s2 n t' s3 s4) <$> facetsXYZ f t

solidThreeOfXYZ ::
  Traversal1' (Solid a n) n
solidThreeOfXYZ =
  facets . facetsThreeOfXYZ

solidLoopXYZ ::
  Traversal1' (Solid a n) n
solidLoopXYZ =
  facets . facetsLoopXYZ

instance Bifunctor Solid where
  bimap f g (Solid s1 s2 n t s3 s4) =
    Solid (fmap f s1) (fmap f s2) n (bimap f g t) (fmap f s3) (fmap f s4)

instance Functor (Solid a) where
  fmap =
    bimap id

instance Bifoldable Solid where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 Solid where
  bifoldMap1 f g (Solid [] s2 _ t s3 s4) =
    foldMap1 f s2 <> bifoldMap1 f g t <> foldMap1 f s3 <> foldMap1 f s4
  bifoldMap1 f g (Solid (s1h:s1t) s2 _ t s3 s4) =
    foldMap1 f (s1h:|s1t) <> foldMap1 f s2 <> bifoldMap1 f g t <> foldMap1 f s3 <> foldMap1 f s4

instance Foldable (Solid a) where
  foldMap =
    foldMap1

instance Foldable1 (Solid a) where
  foldMap1 f (Solid _ _ _ t _ _) =
    foldMap1 f t

instance Bitraversable Solid where
  bitraverse f g (Solid s1 s2 n t s3 s4) =
    (\s1' s2' t' s3' s4' -> Solid s1' s2' n t' s3' s4') <$>
      traverse f s1 <*>
      traverse f s2 <*>
      bitraverse f g t <*>
      traverse f s3 <*>
      traverse f s4

instance Bitraversable1 Solid where
  bitraverse1 f g (Solid [] s2 n t s3 s4) =
    (\s2' t' s3' s4' -> Solid [] s2' n t' s3' s4') <$>
      traverse1 f s2 <.>
      bitraverse1 f g t <.>
      traverse1 f s3 <.>
      traverse1 f s4
  bitraverse1 f g (Solid (s1h:s1t) s2 n t s3 s4) =
    (\s1' s2' t' s3' s4' -> Solid (toList s1') s2' n t' s3' s4') <$>
      traverse1 f (s1h:|s1t) <.>
      traverse1 f s2 <.>
      bitraverse1 f g t <.>
      traverse1 f s3 <.>
      traverse1 f s4

instance Traversable (Solid a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (Solid a) where
  traverse1 f (Solid s1 s2 n t s3 s4) =
    (\t' -> Solid s1 s2 n t' s3 s4) <$>
      traverse1 f t

class AsSolid c a n | c -> a n where
  _Solid :: Prism' c (Solid a n)

instance AsSolid (Solid a n) a n where
  _Solid = id

class HasSolid c a n | c -> a n where
  solid :: Lens' c (Solid a n)
  {-# INLINE solidName #-}
  solidName :: Lens' c (NonEmpty Char)
  solidName = solid . solidName
  {-# INLINE solidSpaces1 #-}
  solidSpaces1 :: Lens' c [a]
  solidSpaces1 = solid . solidSpaces1
  {-# INLINE solidSpaces2 #-}
  solidSpaces2 :: Lens' c (NonEmpty a)
  solidSpaces2 = solid . solidSpaces2
  {-# INLINE solidSpaces3 #-}
  solidSpaces3 :: Lens' c (NonEmpty a)
  solidSpaces3 = solid . solidSpaces3
  {-# INLINE solidSpaces4 #-}
  solidSpaces4 :: Lens' c (NonEmpty a)
  solidSpaces4 = solid . solidSpaces4

instance HasSolid (Solid a n) a n where
  solid =
    id
  {-# INLINE solidName #-}
  solidName f (Solid s1 s2 n t s3 s4) =
    fmap (\n' -> Solid s1 s2 n' t s3 s4) (f n)
  {-# INLINE solidSpaces1 #-}
  solidSpaces1 f (Solid s1 s2 n t s3 s4) =
    fmap (\s1' -> Solid s1' s2 n t s3 s4) (f s1)
  {-# INLINE solidSpaces2 #-}
  solidSpaces2 f (Solid s1 s2 n t s3 s4) =
    fmap (\s2' -> Solid s1 s2' n t s3 s4) (f s2)
  {-# INLINE solidSpaces3 #-}
  solidSpaces3 f (Solid s1 s2 n t s3 s4) =
    fmap (\s3' -> Solid s1 s2 n t s3' s4) (f s3)
  {-# INLINE solidSpaces4 #-}
  solidSpaces4 f (Solid s1 s2 n t s3 s4) =
    fmap (\s4' -> Solid s1 s2 n t s3 s4') (f s4)

instance HasFacets (Solid a n) a n where
  facets f (Solid s1 s2 n t s3 s4) =
    fmap (\t' -> Solid s1 s2 n t' s3 s4) (f t)

-- | Parse the solid of STL excluding trailing space.
parseSolid ::
  (Monad p, CharParsing p) =>
  p a
  -> p n
  -> p (Solid a n)
parseSolid sp np =
  do  s1 <- many sp
      _ <- string "solid"
      s2 <- some1 sp
      n <- some1 (satisfy (not . isSpace))
      t <- parseFacets sp np
      s3 <- some1 sp
      _ <- string "endsolid"
      s4 <- some1 sp
      _ <- string (toList n)
      pure (Solid s1 s2 n t s3 s4)

type Solid' =
  Solid IsoLatin1 Number

-- | Parse the solid of STL excluding trailing space.
--
-- >>> Text.Parsec.parse (parseSolid' <* Text.Parser.Combinators.eof) "solid" "solid abc facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet endsolid abc"
-- Right (Solid [] (Whitespace_ :| []) ('a' :| "bc") (Facets (Facet (Whitespace_ :| []) (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing)) (Loop (Whitespace_ :| []) (Whitespace_ :| []) (Vertex3 (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing)))) (Whitespace_ :| [])) (Whitespace_ :| []) :| [])) (Whitespace_ :| []) (Whitespace_ :| []))
parseSolid' ::
  (Monad p, CharParsing p) =>
  p Solid'
parseSolid' =
  parseSolid parseIsoLatin1 parseNumber

-- | Print the solid of STL excluding trailing space.
printSolid ::
  Printer a
  -> Printer n
  -> Printer (Solid a n)
printSolid pa pn =
  divide
    (\(Solid s1 s2 n t s3 s4) -> ((((((s1, s2), n), t), s3), s4), n))
    (
      divided
        (
          divided
            (
              divided
                (
                  divided
                    (
                      divided
                        (listPrinter pa)
                        (str "solid" <> listPrinter pa)
                    )
                    stringPrinter
                )
                (printFacets pa pn)
            )
            (listPrinter pa)
        )
        (str "endsolid" <> listPrinter pa)
    )
    stringPrinter

-- | Print the solid of STL excluding trailing space.
--
-- >>> printer printSolid' (Solid [] (Data.Char.Space.Whitespace_ :| []) ('a' :| "bc") (Data.STL.Facets.Facets (Data.STL.Facet.Facet (Data.Char.Space.Whitespace_ :| []) (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 :| []) [] Nothing)) (Data.STL.Loop.Loop (Data.Char.Space.Whitespace_ :| []) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Vertex3.Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing)))) (Data.Char.Space.Whitespace_ :| [])) (Data.Char.Space.Whitespace_ :| []) :| [])) (Data.Char.Space.Whitespace_ :| []) (Data.Char.Space.Whitespace_ :| []))
-- "solid abc facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet endsolid abc"
printSolid' ::
  Printer Solid'
printSolid' =
  printSolid printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseSolid ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (Solid a n) -> String -> Maybe (Solid a n))
  -> Prism' String (Solid a n)
printParseSolid ra rn pa pn parser =
  prism'
    (printer (printSolid ra rn))
    (parser (parseSolid pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseSolid' ::
  (CharParsing p, Monad p) =>
  (p Solid' -> String -> Maybe Solid')
  -> Prism' String Solid'
printParseSolid' =
  printParseSolid
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseSolid' ::
  Prism' String Solid'
parsecPrintParseSolid' =
  printParseSolid' (\p s -> preview _Right (Parsec.parse p "Solid" s))

parsecPrintParseSolid ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (Solid a n)
parsecPrintParseSolid ra rn pa pn =
  printParseSolid ra rn pa pn (\p s -> preview _Right (Parsec.parse p "Solid" s))
