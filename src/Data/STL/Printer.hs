{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Data.STL.Printer(
  Printer(..)
, printer
, listPrinter
, tuple2Printer
, tuple3Printer
, tuple4Printer
, eitherPrinter
, showPrinter
, stringPrinter
, charPrinter
, str
, printIsoLatin1
, printSpaceChar
, printfPrinter
, printDecDigit
, printDecDigits
, chr
, modify
, cons
, snoc
, reversePrinter
, upperCase
, lowerCase
, titleCase
) where

import Control.Category ( Category((.)) )
import Control.Applicative ( Applicative(pure) )
import Control.Lens
    ( Contravariant(contramap),
      _Wrapped,
      Rewrapped,
      Wrapped(..),
      view,
      iso,
      (#),
      review,
      over )
import Data.Char ( Char, toLower, toTitle, toUpper )
import Data.Char.Space
    ( AsIsoLatin1(_IsoLatin1),
      AsSpaceChar(_SpaceChar),
      IsoLatin1,
      SpaceChar )
import Data.Digit ( DecDigit, charDecimal )
import Data.Either ( Either, either )
import Data.Foldable ( Foldable(foldMap) )
import Data.Functor.Contravariant.Divisible
    ( Decidable(..), Divisible(..), chosen, divided )
import Data.List ( map, reverse )
import Data.Monoid ( (<>), Monoid(mempty) )
import Data.Semigroup ( Semigroup )
import Data.String ( String )
import Data.Void ( absurd )
import GHC.Show ( Show(show) )
import Text.Printf ( PrintfArg, printf )

newtype Printer a =
  Printer (a -> String)

instance Printer a ~ t =>
  Rewrapped (Printer a) t

instance Wrapped (Printer a) where
  type Unwrapped (Printer a) =
    a -> String
  _Wrapped' =
    iso (\(Printer x) -> x) Printer

instance Semigroup (Printer a) where
  Printer p <> Printer q =
    Printer (p <> q)

instance Monoid (Printer a) where
  mempty =
    Printer mempty

instance Contravariant Printer where
  contramap f (Printer p) =
    Printer (p . f)

instance Divisible Printer where
  divide f (Printer p) (Printer q) =
    Printer (\a -> let (b, c) = f a in p b <> q c)
  conquer =
    Printer (pure mempty)

instance Decidable Printer where
  choose f (Printer p) (Printer q) =
    Printer (either p q . f)
  lose f =
    Printer(absurd . f)

printer ::
  Printer a
  -> a
  -> String
printer =
  view _Wrapped

listPrinter ::
  Foldable t =>
  Printer a
  -> Printer (t a)
listPrinter (Printer p) =
  Printer (foldMap p)

tuple2Printer ::
  Printer a
  -> Printer b
  -> Printer (a, b)
tuple2Printer =
  divided

tuple3Printer ::
  Printer a
  -> Printer b
  -> Printer c
  -> Printer (a, b, c)
tuple3Printer p q =
  divide (\(a, b, c) -> ((a, b), c)) (tuple2Printer p q)

tuple4Printer ::
  Printer a
  -> Printer b
  -> Printer c
  -> Printer d
  -> Printer (a, b, c, d)
tuple4Printer p q r =
  divide (\(a, b, c, d) -> ((a, b, c), d)) (tuple3Printer p q r)

eitherPrinter ::
  Printer a
  -> Printer b
  -> Printer (Either a b)
eitherPrinter =
  chosen

showPrinter ::
  Show a =>
  Printer a
showPrinter =
  Printer show

stringPrinter ::
  Foldable t =>
  Printer (t Char)
stringPrinter =
  listPrinter charPrinter

charPrinter ::
  Printer Char
charPrinter =
  Printer pure

str ::
  String
  -> Printer a
str =
  Printer . pure

printIsoLatin1 ::
  Printer IsoLatin1
printIsoLatin1 =
  Printer (pure . review _IsoLatin1)

printSpaceChar ::
  Printer SpaceChar
printSpaceChar =
  Printer (pure . review _SpaceChar)

printfPrinter ::
  PrintfArg a =>
  String
  -> Printer a
printfPrinter s =
  Printer (printf s)

printDecDigit ::
  Printer DecDigit
printDecDigit =
  Printer (pure . (charDecimal #))

printDecDigits ::
  Foldable t =>
  Printer (t DecDigit)
printDecDigits =
  listPrinter printDecDigit

chr ::
  Char
  -> Printer a
chr =
  str . pure

modify ::
  (String -> String)
  -> Printer a
  -> Printer a
modify f =
  over _Wrapped (f .)

cons ::
  Char
  -> Printer a
  -> Printer a
cons c =
  modify (c:)

snoc ::
  Char
  -> Printer a
  -> Printer a
snoc c =
  modify (<> [c])

reversePrinter ::
  Printer a
  -> Printer a
reversePrinter =
  modify reverse

upperCase ::
  Printer a
  -> Printer a
upperCase =
  modify (map toUpper)

lowerCase ::
  Printer a
  -> Printer a
lowerCase =
  modify (map toLower)

titleCase ::
  Printer a
  -> Printer a
titleCase =
  modify (map toTitle)
