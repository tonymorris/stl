{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.Vertex3(
  Vertex3(..)
, vertex3Each
, vertex3Spaces
, vertex3XYZ
, HasVertex3(..)
, AsVertex3(..)
, parseVertex3
, Vertex3'
, parseVertex3'
, printVertex3
, printVertex3'
, printParseVertex3
, printParseVertex3'
, parsecPrintParseVertex3
, parsecPrintParseVertex3'
) where

import Control.Applicative ( Applicative((<*>), pure) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism', Lens', preview, _Right, prism', Traversal1 )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Apply ( Apply((<.>)) )
import Data.Functor.Contravariant.Divisible
    ( Divisible(divide), divided )
import Data.List.NonEmpty ( NonEmpty(..) )
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer ( Printer, printer, printIsoLatin1 )
import Data.STL.Vertex
    ( Vertex(..), vertexSpaces, vertexXYZ, parseVertex, printVertex )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing )

data Vertex3 a n =
  Vertex3
    (Vertex a n)
    (Vertex a n)
    (Vertex a n)
  deriving (Eq, Ord, Show, Generic)

vertex3Each ::
  Traversal1 (Vertex3 a n) (Vertex3 b n') (Vertex a n) (Vertex b n')
vertex3Each f (Vertex3 v1 v2 v3) =
  Vertex3 <$> f v1 <.> f v2 <.> f v3

vertex3Spaces ::
  Traversal1 (Vertex3 a n) (Vertex3 b n) (NonEmpty a) (NonEmpty b)
vertex3Spaces =
  vertex3Each . vertexSpaces

vertex3XYZ ::
  Traversal1 (Vertex3 a n) (Vertex3 a n') n n'
vertex3XYZ =
  vertex3Each . vertexXYZ

instance Bifunctor Vertex3 where
  bimap f g (Vertex3 v1 v2 v3) =
    Vertex3 (bimap f g v1) (bimap f g v2) (bimap f g v3)

instance Functor (Vertex3 a) where
  fmap =
    bimap id

instance Bifoldable Vertex3 where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 Vertex3 where
  bifoldMap1 f g (Vertex3 v1 v2 v3) =
    bifoldMap1 f g v1 <> bifoldMap1 f g v2 <> bifoldMap1 f g v3

instance Foldable (Vertex3 a) where
  foldMap =
    foldMap1

instance Foldable1 (Vertex3 a) where
  foldMap1 f (Vertex3 v1 v2 v3) =
    foldMap1 f v1 <> foldMap1 f v2 <> foldMap1 f v3

instance Bitraversable Vertex3 where
  bitraverse f g (Vertex3 v1 v2 v3) =
    Vertex3 <$>
      bitraverse f g v1 <*>
      bitraverse f g v2 <*>
      bitraverse f g v3

instance Bitraversable1 Vertex3 where
  bitraverse1 f g (Vertex3 v1 v2 v3) =
    Vertex3 <$>
      bitraverse1 f g v1 <.>
      bitraverse1 f g v2 <.>
      bitraverse1 f g v3

instance Traversable (Vertex3 a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (Vertex3 a) where
  traverse1 f (Vertex3 v1 v2 v3) =
    Vertex3 <$>
      traverse1 f v1 <.>
      traverse1 f v2 <.>
      traverse1 f v3

class AsVertex3 c a n | c -> a n where
  _Vertex3 :: Prism' c (Vertex3 a n)

instance AsVertex3 (Vertex3 a n) a n where
  _Vertex3 = id

class HasVertex3 c a n | c -> a n where
  vertex3 :: Lens' c (Vertex3 a n)
  {-# INLINE vertex3A #-}
  vertex3A :: Lens' c (Vertex a n)
  vertex3A = vertex3 . vertex3A
  {-# INLINE vertex3B #-}
  vertex3B :: Lens' c (Vertex a n)
  vertex3B = vertex3 . vertex3B
  {-# INLINE vertex3C #-}
  vertex3C :: Lens' c (Vertex a n)
  vertex3C = vertex3 . vertex3C

instance HasVertex3 (Vertex3 a n) a n where
  vertex3 =
    id
  {-# INLINE vertex3A #-}
  vertex3A f (Vertex3 v1 v2 v3) =
    fmap (\v1' -> Vertex3 v1' v2 v3) (f v1)
  {-# INLINE vertex3B #-}
  vertex3B f (Vertex3 v1 v2 v3) =
    fmap (\v2' -> Vertex3 v1 v2' v3) (f v2)
  {-# INLINE vertex3C #-}
  vertex3C f (Vertex3 v1 v2 v3) =
    fmap (\v3' -> Vertex3 v1 v2 v3') (f v3)

-- | Parse three vertices of STL, including trailing space, and space between each vertex
parseVertex3 ::
  (CharParsing p, Monad p) =>
  p a
  -> p n
  -> p (Vertex3 a n)
parseVertex3 sp np =
  Vertex3 <$> parseVertex sp np <*>  parseVertex sp np <*>  parseVertex sp np

type Vertex3' =
  Vertex3 IsoLatin1 Number


-- | Parse three vertices of STL, including trailing space, and space between each vertex
--
-- >>> Text.Parsec.parse (parseVertex3' <* Text.Parser.Combinators.eof) "vertex3" "\n vertex 1 2 3\nvertex 4 5 6\nvertex 7 8 9"
-- Right (Vertex3 (Vertex (LineFeed_ :| [Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing))) (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing))) (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit7 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit8 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| []) [] Nothing))))
-- >>> Text.Parsec.parse (parseVertex3' <* Text.Parser.Combinators.eof) "vertex3" "\n vertex 1 2 3\n   \n  vertex 4 5 6\nvertex 7 8 9"
-- Right (Vertex3 (Vertex (LineFeed_ :| [Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing))) (Vertex (LineFeed_ :| [Whitespace_,Whitespace_,Whitespace_,LineFeed_,Whitespace_,Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing))) (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit7 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit8 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| []) [] Nothing))))
-- >>> Text.Parsec.parse parseVertex3' "vertex3" "\n vertex 1 2 3\n   \n  vertex 4 5 6  \n  vertex 7 8 9  "
-- Right (Vertex3 (Vertex (LineFeed_ :| [Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing))) (Vertex (LineFeed_ :| [Whitespace_,Whitespace_,Whitespace_,LineFeed_,Whitespace_,Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing))) (Vertex (Whitespace_ :| [Whitespace_,LineFeed_,Whitespace_,Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit7 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit8 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| []) [] Nothing))))
-- >>> Text.Parsec.parse (parseVertex3' <* Text.Parser.Combinators.eof) "vertex3" "  vertex 1 2 3\nvertex 4 5 6\nvertex 7 8 9"
-- Right (Vertex3 (Vertex (Whitespace_ :| [Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing))) (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing))) (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit7 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit8 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| []) [] Nothing))))
parseVertex3' ::
  (Monad p, CharParsing p) =>
  p Vertex3'
parseVertex3' =
  parseVertex3 parseIsoLatin1 parseNumber

-- | Print three vertices of STL, including trailing space, and space between each vertex
printVertex3 ::
  Printer a
  -> Printer n
  -> Printer (Vertex3 a n)
printVertex3 pa pn =
  divide
    (\(Vertex3 v1 v2 v3) -> ((v1, v2), v3))
    (
      divided
        (printVertex pa pn)
        (printVertex pa pn)
    )
    (printVertex pa pn)

-- | Print three vertices of STL, including trailing space, and space between each vertex
--
-- >>> printer printVertex3' (Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ :| [Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing))) (Vertex (Data.Char.Space.LineFeed_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 :| []) [] Nothing))) (Vertex (Data.Char.Space.LineFeed_ :| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit7 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit8 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 :| []) [] Nothing))))
-- "\n vertex 1 2 3\nvertex 4 5 6\nvertex 7 8 9"
-- >>> printer printVertex3' (Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit7 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit8 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 Data.List.NonEmpty.:| []) [] Nothing))))
-- "\n vertex 1 2 3\n   \n  vertex 4 5 6\nvertex 7 8 9"
-- >>> printer printVertex3' (Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ :| [Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 :| []) [] Nothing))) (Vertex (Data.Char.Space.LineFeed_ :| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 :| []) [] Nothing))) (Vertex (Data.Char.Space.Whitespace_ :| [Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit7 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit8 :| []) [] Nothing) (Data.Char.Space.Whitespace_ :| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 :| []) [] Nothing))))
-- "\n vertex 1 2 3\n   \n  vertex 4 5 6  \n  vertex 7 8 9"
-- >>> printer printVertex3' (Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit7 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit8 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 Data.List.NonEmpty.:| []) [] Nothing))))
-- "  vertex 1 2 3\nvertex 4 5 6\nvertex 7 8 9"
printVertex3' ::
  Printer Vertex3'
printVertex3' =
  printVertex3 printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseVertex3 ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (Vertex3 a n) -> String -> Maybe (Vertex3 a n))
  -> Prism' String (Vertex3 a n)
printParseVertex3 ra rn pa pn parser =
  prism'
    (printer (printVertex3 ra rn))
    (parser (parseVertex3 pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseVertex3' ::
  (CharParsing p, Monad p) =>
  (p Vertex3' -> String -> Maybe Vertex3')
  -> Prism' String Vertex3'
printParseVertex3' =
  printParseVertex3
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseVertex3' ::
  Prism' String Vertex3'
parsecPrintParseVertex3' =
  printParseVertex3' (\p s -> preview _Right (Parsec.parse p "Vertex3" s))

parsecPrintParseVertex3 ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (Vertex3 a n)
parsecPrintParseVertex3 ra rn pa pn =
  printParseVertex3 ra rn pa pn (\p s -> preview _Right (Parsec.parse p "Vertex3" s))
