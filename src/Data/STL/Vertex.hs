{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.Vertex(
  Vertex(..)
, vertexSpaces
, vertexXYZ
, HasVertex(..)
, AsVertex(..)
, parseVertex
, Vertex'
, parseVertex'
, printVertex
, printVertex'
, printParseVertex
, printParseVertex'
, parsecPrintParseVertex
, parsecPrintParseVertex'
) where

import Control.Applicative ( Applicative((<*>), pure) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism', Lens', preview, _Right, prism', Traversal1 )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Apply ( Apply((<.>)) )
import Data.Functor.Contravariant.Divisible( Divisible(divide) )
import Data.List.NonEmpty(NonEmpty, some1)
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.ThreeOf
    ( HasThreeOf(threeOf),
      ThreeOf,
      threeOfSpaces,
      threeOfXYZ,
      parseThreeOf,
      printThreeOf )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer
    ( Printer, printer, listPrinter, str, printIsoLatin1 )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing(string) )

data Vertex a n =
  Vertex
    (NonEmpty a) -- leadingSpaces
    (ThreeOf a n)
  deriving (Eq, Ord, Show, Generic)

vertexSpaces ::
  Traversal1 (Vertex a n) (Vertex b n) (NonEmpty a) (NonEmpty b)
vertexSpaces f (Vertex s1 x) =
  Vertex <$> f s1 <.> threeOfSpaces f x

vertexXYZ ::
  Traversal1 (Vertex a n) (Vertex a n') n n'
vertexXYZ f (Vertex s1 x) =
  Vertex s1 <$> threeOfXYZ f x

instance Bifunctor Vertex where
  bimap f g (Vertex s1 x) =
    Vertex (fmap f s1) (bimap f g x)

instance Functor (Vertex a) where
  fmap =
    bimap id

instance Bifoldable Vertex where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 Vertex where
  bifoldMap1 f g (Vertex s1 x) =
    foldMap1 f s1 <> foldMap1 g x

instance Foldable (Vertex a) where
  foldMap =
    foldMap1

instance Foldable1 (Vertex a) where
  foldMap1 f (Vertex _ x) =
    foldMap1 f x

instance Bitraversable Vertex where
  bitraverse f g (Vertex s1 x) =
    Vertex <$>
      traverse f s1 <*>
      bitraverse f g x

instance Bitraversable1 Vertex where
  bitraverse1 f g (Vertex s1 x) =
    Vertex <$>
      traverse1 f s1 <.>
      bitraverse1 f g x

instance Traversable (Vertex a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (Vertex a) where
  traverse1 f (Vertex s x) =
    Vertex s <$>
      traverse1 f x

class AsVertex c a n | c -> a n where
  _Vertex ::
    Prism' c (Vertex a n)

instance AsVertex (Vertex a n) a n where
  _Vertex =
    id

class HasVertex c a n | c -> a n where
  vertex :: Lens' c (Vertex a n)
  {-# INLINE vertexSpaces1 #-}
  vertexSpaces1 :: Lens' c (NonEmpty a)
  vertexSpaces1 = vertex . vertexSpaces1

instance HasVertex (Vertex a n) a n where
  vertex =
    id
  {-# INLINE vertexSpaces1 #-}
  vertexSpaces1 f (Vertex s1 x) =
    fmap (\s1' -> Vertex s1' x) (f s1)

instance HasThreeOf (Vertex a n) a n where
  threeOf f (Vertex s1 x) =
    fmap (\x' -> Vertex s1 x') (f x)

-- | Parse a single vertex line of STL including trailing space
parseVertex ::
  (CharParsing p, Monad p) =>
  p a
  -> p n
  -> p (Vertex a n)
parseVertex sp np =
  do  s <- some1 sp
      _ <- string "vertex"
      d3 <- parseThreeOf sp np
      pure (Vertex s d3)

type Vertex' =
  Vertex IsoLatin1 Number

-- | Parse a single vertex line of STL including trailing space
--
-- >>> Text.Parsec.parse (parseVertex' <* Text.Parser.Combinators.eof) "vertex" " vertex 10.0 92.3 5.1"
-- Right (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| [DecDigit0]) [DecDigit0] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [DecDigit1] Nothing)))
-- >>> Text.Parsec.parse (parseVertex' <* Text.Parser.Combinators.eof) "vertex" "   vertex 10.0 -92.3 5.1"
-- Right (Vertex (Whitespace_ :| [Whitespace_,Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| [DecDigit0]) [DecDigit0] Nothing) (Whitespace_ :| []) (Number Minus (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [DecDigit1] Nothing)))
-- >>> Text.Parsec.parse (parseVertex' <* Text.Parser.Combinators.eof) "vertex" "\n  \n  vertex     \t 10.0 -92.3 1e+10"
-- Right (Vertex (LineFeed_ :| [Whitespace_,Whitespace_,LineFeed_,Whitespace_,Whitespace_]) (ThreeOf (Whitespace_ :| [Whitespace_,Whitespace_,Whitespace_,Whitespace_,HorizontalTab_,Whitespace_]) (Number Nosign (DecDigit1 :| [DecDigit0]) [DecDigit0] Nothing) (Whitespace_ :| []) (Number Minus (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] (Just (E False Plus (DecDigit1 :| [DecDigit0]))))))
-- >>> Text.Parsec.parse parseVertex' "vertex" "  vertex -5.6e+10 -92.3 1e+10    \n"
-- Right (Vertex (Whitespace_ :| [Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Minus (DecDigit5 :| []) [DecDigit6] (Just (E False Plus (DecDigit1 :| [DecDigit0])))) (Whitespace_ :| []) (Number Minus (DecDigit9 :| [DecDigit2]) [DecDigit3] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] (Just (E False Plus (DecDigit1 :| [DecDigit0]))))))
parseVertex' ::
  (Monad p, CharParsing p) =>
  p Vertex'
parseVertex' =
  parseVertex parseIsoLatin1 parseNumber

printVertex ::
  Printer a
  -> Printer n
  -> Printer (Vertex a n)
printVertex pa pn =
  divide
    (\(Vertex s1 x) -> (s1, x))
    (listPrinter pa)
    (str "vertex" <> printThreeOf pa pn)

-- | Print a single vertex line of STL including trailing space
--
-- >>> printer printVertex' (Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [Data.Digit.DecDigit1] Nothing)))
-- " vertex 10.0 92.3 5.1"
-- >>> printer printVertex' (Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [Data.Digit.DecDigit1] Nothing)))
-- "   vertex 10.0 -92.3 5.1"
-- >>> printer printVertex' (Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.HorizontalTab_,Data.Char.Space.Whitespace_]) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]) [Data.Digit.DecDigit0] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] (Just (Data.STL.Number.E False Data.STL.Number.Plus (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]))))))
-- "\n  \n  vertex     \t 10.0 -92.3 1e+10"
-- >>> printer printVertex' (Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [Data.Digit.DecDigit6] (Just (Data.STL.Number.E False Data.STL.Number.Plus (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0])))) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit9 Data.List.NonEmpty.:| [Data.Digit.DecDigit2]) [Data.Digit.DecDigit3] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] (Just (Data.STL.Number.E False Data.STL.Number.Plus (Data.Digit.DecDigit1 Data.List.NonEmpty.:| [Data.Digit.DecDigit0]))))))
-- "  vertex -5.6e+10 -92.3 1e+10"
printVertex' ::
  Printer Vertex'
printVertex' =
  printVertex printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseVertex ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (Vertex a n) -> String -> Maybe (Vertex a n))
  -> Prism' String (Vertex a n)
printParseVertex ra rn pa pn parser =
  prism'
    (printer (printVertex ra rn))
    (parser (parseVertex pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseVertex' ::
  (CharParsing p, Monad p) =>
  (p Vertex' -> String -> Maybe Vertex')
  -> Prism' String Vertex'
printParseVertex' =
  printParseVertex
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseVertex' ::
  Prism' String Vertex'
parsecPrintParseVertex' =
  printParseVertex' (\p s -> preview _Right (Parsec.parse p "Vertex" s))

parsecPrintParseVertex ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (Vertex a n)
parsecPrintParseVertex ra rn pa pn =
  printParseVertex ra rn pa pn (\p s -> preview _Right (Parsec.parse p "Vertex" s))

