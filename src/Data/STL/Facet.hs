{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.Facet(
  Facet(..)
, facetVertexEach
, facetSpaces
, facetXYZ
, facetThreeOfXYZ
, facetLoopXYZ
, HasFacet(..)
, AsFacet(..)
, parseFacet
, Facet'
, parseFacet'
, printFacet
, printFacet'
, printParseFacet
, printParseFacet'
, parsecPrintParseFacet
, parsecPrintParseFacet'
) where

import Control.Applicative ( Applicative(pure, (<*>)) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism', Lens', preview, _Right, prism', Traversal1, Traversal1' )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Apply ( Apply((<.>)) )
import Data.Functor.Contravariant.Divisible
    ( Divisible(divide), divided )
import Data.List.NonEmpty(NonEmpty, some1)
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.ThreeOf
    ( HasThreeOf(threeOf),
      ThreeOf,
      threeOfSpaces,
      threeOfXYZ,
      parseThreeOf,
      printThreeOf )
import Data.STL.Loop
    ( loopSpaces,
      loopXYZ,
      parseLoop,
      printLoop,
      HasLoop(loop),
      Loop,
      loopVertexEach )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer
    ( Printer(..), printer, listPrinter, str, printIsoLatin1 )
import Data.STL.Vertex ( Vertex )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing(string) )
import Prelude hiding ((.), id)

data Facet a n =
  Facet
    (NonEmpty a)
    (NonEmpty a)
    (ThreeOf a n)
    (Loop a n)
    (NonEmpty a)
  deriving (Eq, Ord, Show, Generic)

facetVertexEach ::
  Traversal1' (Facet a n) (Vertex a n)
facetVertexEach =
  let loop' f (Facet s1 s2 d l s3) =
        fmap (\l' -> Facet s1 s2 d l' s3) (f l)
  in  loop' . loopVertexEach

facetSpaces ::
  Traversal1 (Facet a n) (Facet b n) (NonEmpty a) (NonEmpty b)
facetSpaces f (Facet s1 s2 d l s3) =
  Facet <$> f s1 <.> f s2 <.> threeOfSpaces f d <.> loopSpaces f l <.> f s3

facetXYZ ::
  Traversal1 (Facet a n) (Facet a n') n n'
facetXYZ f (Facet s1 s2 d l s3) =
  (\d' l' -> Facet s1 s2 d' l' s3) <$> threeOfXYZ f d <.> loopXYZ f l

facetThreeOfXYZ ::
  Traversal1' (Facet a n) n
facetThreeOfXYZ f (Facet s1 s2 d l s3) =
  (\d' -> Facet s1 s2 d' l s3) <$> threeOfXYZ f d

facetLoopXYZ ::
  Traversal1' (Facet a n) n
facetLoopXYZ f (Facet s1 s2 d l s3) =
  (\l' -> Facet s1 s2 d l' s3) <$> loopXYZ f l

instance Bifunctor Facet where
  bimap f g (Facet s1 s2 d l s3) =
    Facet (fmap f s1) (fmap f s2) (bimap f g d) (bimap f g l) (fmap f s3)

instance Functor (Facet a) where
  fmap =
    bimap id

instance Bifoldable Facet where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 Facet where
  bifoldMap1 f g (Facet s1 s2 d l s3) =
    foldMap1 f s1 <> foldMap1 f s2 <> bifoldMap1 f g d <> bifoldMap1 f g l <> foldMap1 f s3

instance Foldable (Facet a) where
  foldMap =
    foldMap1

instance Foldable1 (Facet a) where
  foldMap1 f (Facet _ _ d l _) =
    foldMap1 f d <> foldMap1 f l

instance Bitraversable Facet where
  bitraverse f g (Facet s1 s2 d l s3) =
    Facet <$>
      traverse f s1 <*>
      traverse f s2 <*>
      bitraverse f g d <*>
      bitraverse f g l <*>
      traverse f s3

instance Bitraversable1 Facet where
  bitraverse1 f g (Facet s1 s2 d l s3) =
    Facet <$>
      traverse1 f s1 <.>
      traverse1 f s2 <.>
      bitraverse1 f g d <.>
      bitraverse1 f g l <.>
      traverse1 f s3

instance Traversable (Facet a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (Facet a) where
  traverse1 f (Facet s1 s2 d l s3) =
    (\d' l' -> Facet s1 s2 d' l' s3) <$>
      traverse1 f d <.>
      traverse1 f l

class AsFacet c a n | c -> a n where
  _Facet :: Prism' c (Facet a n)

instance AsFacet (Facet a n) a n where
  _Facet = id

class HasFacet c a n | c -> a n where
  facet :: Lens' c (Facet a n)
  {-# INLINE facetSpaces1 #-}
  facetSpaces1 :: Lens' c (NonEmpty a)
  facetSpaces1 = facet . facetSpaces1
  {-# INLINE facetSpaces2 #-}
  facetSpaces2 :: Lens' c (NonEmpty a)
  facetSpaces2 = facet . facetSpaces2
  {-# INLINE facetSpaces3 #-}
  facetSpaces3 :: Lens' c (NonEmpty a)
  facetSpaces3 = facet . facetSpaces3

instance HasFacet (Facet a n) a n where
  facet =
    id
  {-# INLINE facetSpaces1 #-}
  facetSpaces1 f (Facet s1 s2 d l s3) =
    fmap (\s1' -> Facet s1' s2 d l s3) (f s1)
  {-# INLINE facetSpaces2 #-}
  facetSpaces2 f (Facet s1 s2 d l s3) =
    fmap (\s2' -> Facet s1 s2' d l s3) (f s2)
  {-# INLINE facetSpaces3 #-}
  facetSpaces3 f (Facet s1 s2 d l s3) =
    fmap (\s3' -> Facet s1 s2 d l s3') (f s3)

instance HasThreeOf (Facet a n) a n where
  threeOf f (Facet s1 s2 d l s3) =
    fmap (\d' -> Facet s1 s2 d' l s3) (f d)

instance HasLoop (Facet a n) a n where
  loop f (Facet s1 s2 d l s3) =
    fmap (\l' -> Facet s1 s2 d l' s3) (f l)

-- | Parse the "facet normal" up to "endfacet" of STL including leading space.
parseFacet ::
  (CharParsing p, Monad p) =>
  p a
  -> p n
  -> p (Facet a n)
parseFacet sp np =
  do  s1 <- some1 sp
      _ <- string "facet"
      s2 <- some1 sp
      _ <- string "normal"
      d <- parseThreeOf sp np
      l <- parseLoop sp np
      s3 <- some1 sp
      _ <- string "endfacet"
      pure (Facet s1 s2 d l s3)

type Facet' =
  Facet IsoLatin1 Number

-- | Parse the "facet normal" up to "endfacet" of STL including leading space.
--
-- >>> Text.Parsec.parse (parseFacet' <* Text.Parser.Combinators.eof) "facet" " facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet"
-- Right (Facet (Whitespace_ :| []) (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing)) (Loop (Whitespace_ :| []) (Whitespace_ :| []) (Vertex3 (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing))) (Vertex (Whitespace_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing)))) (Whitespace_ :| [])) (Whitespace_ :| []))
parseFacet' ::
  (CharParsing p, Monad p) =>
   p Facet'
parseFacet' =
  parseFacet parseIsoLatin1 parseNumber

-- | Print the "facet normal" up to "endfacet" of STL including leading space.
printFacet ::
  Printer a
  -> Printer n
  -> Printer (Facet a n)
printFacet pa pn =
  divide
    (\(Facet s1 s2 d l s3) -> ((((s1, s2), d), l), s3))
    (
      divided
        (
          divided
          (
            divided
              (listPrinter pa)
              (str "facet" <> listPrinter pa)
          )
            (str "normal" <> printThreeOf pa pn)
        )
        (printLoop pa pn)
    )
    (listPrinter pa <> str "endfacet")

-- | Print the "facet normal" up to "endfacet" of STL including leading space.
--
-- >>> printer printFacet' (Facet (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 Data.List.NonEmpty.:| []) [] Nothing)) (Data.STL.Loop.Loop (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Vertex3.Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing)))) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [])) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []))
-- " facet normal 4 5 6 outer loop vertex 1 1 1 vertex 2 2 2 vertex 3 3 3 endloop endfacet"
printFacet' ::
  Printer Facet'
printFacet' =
  printFacet printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseFacet ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (Facet a n) -> String -> Maybe (Facet a n))
  -> Prism' String (Facet a n)
printParseFacet ra rn pa pn parser =
  prism'
    (printer (printFacet ra rn))
    (parser (parseFacet pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseFacet' ::
  (CharParsing p, Monad p) =>
  (p Facet' -> String -> Maybe Facet')
  -> Prism' String Facet'
printParseFacet' =
  printParseFacet
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseFacet' ::
  Prism' String Facet'
parsecPrintParseFacet' =
  printParseFacet' (\p s -> preview _Right (Parsec.parse p "Facet" s))

parsecPrintParseFacet ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (Facet a n)
parsecPrintParseFacet ra rn pa pn =
  printParseFacet ra rn pa pn (\p s -> preview _Right (Parsec.parse p "Facet" s))
