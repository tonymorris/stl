{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Data.STL.File(
  readSTLFile
, readSTLFile'
, parsecReadSTLFile
, parsecReadSTLFile'
, writeSTLFile
, writeSTLFile'
) where

import Control.Monad ( Monad )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Either ( Either )
import Data.Functor ( (<$>) )
import Data.Semigroup ( Semigroup((<>)) )
import Data.String ( String )
import System.IO ( IO, FilePath, readFile, writeFile )
import Data.STL.Number ( Number, parseNumber )
import Data.STL.Printer ( printer, Printer )
import Data.STL.STL ( parseSTL, printSTL, printSTL', STL, STL' )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( ParseError, Parsec )
import Text.Parser.Char ( CharParsing )

readSTLFile ::
  (Monad p, CharParsing p) =>
  p a
  -> p n
  -> (p (STL a n) -> String -> x)
  -> FilePath
  -> IO x
readSTLFile pa pn parser file =
  parser (parseSTL pa pn) <$> readFile file

readSTLFile' ::
  (CharParsing p, Monad p) =>
  (p STL' -> String -> x)
  -> FilePath
  -> IO x
readSTLFile' =
  readSTLFile parseIsoLatin1 parseNumber

parsecReadSTLFile ::
  Parsec String () a
  -> Parsec String () n
  -> FilePath
  -> IO (Either ParseError (STL a n))
parsecReadSTLFile pa pn file =
  readSTLFile pa pn (\p s -> Parsec.parse p ("parsecReadSTLFile: " <> file) s) file

parsecReadSTLFile' ::
  FilePath
  -> IO (Either ParseError (STL IsoLatin1 Number))
parsecReadSTLFile' file =
  readSTLFile parseIsoLatin1 parseNumber (\p s -> Parsec.parse p ("parsecReadSTLFile': " <> file) s) file

writeSTLFile ::
  Printer a
  -> Printer n
  -> FilePath
  -> STL a n
  -> IO ()
writeSTLFile ra rn file s =
  writeFile file (printer (printSTL ra rn) s)

writeSTLFile' ::
  FilePath
  -> STL'
  -> IO ()
writeSTLFile' file s =
  writeFile file (printer printSTL' s)
