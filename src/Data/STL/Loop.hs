{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

module Data.STL.Loop(
  Loop(..)
, loopVertexEach
, loopSpaces
, loopXYZ
, HasLoop(..)
, AsLoop(..)
, parseLoop
, Loop'
, parseLoop'
, printLoop
, printLoop'
, printParseLoop
, printParseLoop'
, parsecPrintParseLoop
, parsecPrintParseLoop'
) where

import Control.Applicative ( Applicative(pure, (<*>)) )
import Control.Category ( Category(id, (.)) )
import Control.Lens
    ( Prism', Lens', preview, _Right, prism', Traversal1 )
import Control.Monad ( Monad )
import Data.Bifoldable ( Bifoldable(bifoldMap) )
import Data.Bifunctor ( Bifunctor(bimap) )
import Data.Bitraversable ( Bitraversable(..) )
import Data.Char.Space ( parseIsoLatin1, IsoLatin1 )
import Data.Eq ( Eq )
import Data.Foldable ( Foldable(foldMap) )
import Data.Functor ( Functor(fmap), (<$>) )
import Data.Functor.Apply ( Apply((<.>)) )
import Data.Functor.Contravariant.Divisible
    ( Divisible(divide), divided )
import Data.List.NonEmpty(NonEmpty, some1)
import Data.Maybe ( Maybe )
import Data.Ord ( Ord )
import Data.Semigroup ( Semigroup((<>)) )
import Data.Semigroup.Foldable ( Foldable1(foldMap1) )
import Data.Semigroup.Foldable.Class ( Bifoldable1(bifoldMap1) )
import Data.Semigroup.Traversable ( Traversable1(traverse1) )
import Data.Semigroup.Traversable.Class
    ( Bitraversable1(bitraverse1) )
import Data.STL.Number ( Number, parseNumber, printNumber )
import Data.STL.Printer
    ( Printer, printer, listPrinter, str, printIsoLatin1 )
import Data.STL.Vertex ( Vertex )
import Data.STL.Vertex3
    ( parseVertex3,
      printVertex3,
      vertex3Each,
      vertex3Spaces,
      vertex3XYZ,
      HasVertex3(vertex3),
      Vertex3 )
import Data.String ( String )
import Data.Traversable ( Traversable(traverse) )
import GHC.Generics ( Generic )
import GHC.Show ( Show )
import qualified Text.Parsec as Parsec(parse)
import Text.Parsec ( Parsec )
import Text.Parser.Char ( CharParsing(string) )

data Loop a n =
  Loop
    (NonEmpty a)
    (NonEmpty a)
    (Vertex3 a n)
    (NonEmpty a)
  deriving (Eq, Ord, Show, Generic)

loopVertexEach ::
  Traversal1 (Loop a n) (Loop a n') (Vertex a n) (Vertex a n')
loopVertexEach =
  let vertex3' f (Loop s1 s2 v s3) =
        fmap (\v' -> Loop s1 s2 v' s3) (f v)
  in  vertex3' . vertex3Each

loopSpaces ::
  Traversal1 (Loop a n) (Loop b n) (NonEmpty a) (NonEmpty b)
loopSpaces f (Loop s1 s2 x s3) =
  Loop <$> f s1 <.> f s2 <.> vertex3Spaces f x <.> f s3

loopXYZ ::
  Traversal1 (Loop a n) (Loop a n') n n'
loopXYZ f (Loop s1 s2 x s3) =
  (\x' -> Loop s1 s2 x' s3) <$> vertex3XYZ f x

instance Bifunctor Loop where
  bimap f g (Loop s1 s2 v s3) =
    Loop (fmap f s1) (fmap f s2) (bimap f g v) (fmap f s3)

instance Functor (Loop a) where
  fmap =
    bimap id

instance Bifoldable Loop where
  bifoldMap =
    bifoldMap1

instance Bifoldable1 Loop where
  bifoldMap1 f g (Loop s1 s2 v s3) =
    foldMap1 f s1 <> foldMap1 f s2 <> bifoldMap1 f g v <> foldMap1 f s3

instance Foldable (Loop a) where
  foldMap =
    foldMap1

instance Foldable1 (Loop a) where
  foldMap1 f (Loop _ _ v _) =
    foldMap1 f v

instance Bitraversable Loop where
  bitraverse f g (Loop s1 s2 v s3) =
    Loop <$>
      traverse f s1 <*>
      traverse f s2 <*>
      bitraverse f g v <*>
      traverse f s3

instance Bitraversable1 Loop where
  bitraverse1 f g (Loop s1 s2 v s3) =
    Loop <$>
      traverse1 f s1 <.>
      traverse1 f s2 <.>
      bitraverse1 f g v <.>
      traverse1 f s3

instance Traversable (Loop a) where
  traverse f =
    bitraverse pure f

instance Traversable1 (Loop a) where
  traverse1 f (Loop s1 s2 v s3) =
    (\v' -> Loop s1 s2 v' s3) <$>
      traverse1 f v

class AsLoop c a n | c -> a n where
  _Loop :: Prism' c (Loop a n)

instance AsLoop (Loop a n) a n where
  _Loop = id

class HasLoop c a n | c -> a n where
  loop :: Lens' c (Loop a n)
  {-# INLINE loopSpaces1 #-}
  loopSpaces1 :: Lens' c (NonEmpty a)
  loopSpaces1 = loop . loopSpaces1
  {-# INLINE loopSpaces2 #-}
  loopSpaces2 :: Lens' c (NonEmpty a)
  loopSpaces2 = loop . loopSpaces2
  {-# INLINE loopSpaces3 #-}
  loopSpaces3 :: Lens' c (NonEmpty a)
  loopSpaces3 = loop . loopSpaces3

instance HasLoop (Loop a n) a n where
  loop =
    id
  {-# INLINE loopSpaces1 #-}
  loopSpaces1 f (Loop s1 s2 v s3) =
    fmap (\s1' -> Loop s1' s2 v s3) (f s1)
  loopSpaces2 f (Loop s1 s2 v s3) =
    fmap (\s2' -> Loop s1 s2' v s3) (f s2)
  loopSpaces3 f (Loop s1 s2 v s3) =
    fmap (\s3' -> Loop s1 s2 v s3') (f s3)

instance HasVertex3 (Loop a n) a n where
  vertex3 f (Loop s1 s2 v s3) =
    fmap (\v' -> Loop s1 s2 v' s3) (f v)

-- | The "outer loop" up to "endloop" of STL including leading spaces
parseLoop ::
  (CharParsing p, Monad p) =>
  p a
  -> p n
  -> p (Loop a n)
parseLoop sp np =
  do  s1 <- some1 sp
      _ <- string "outer"
      s2 <- some1 sp
      _ <- string "loop"
      v3 <- parseVertex3 sp np
      s3 <- some1 sp
      _ <- string "endloop"
      pure (Loop s1 s2 v3 s3)

type Loop' =
  Loop IsoLatin1 Number

-- | Parse the "outer loop" up to "endloop" of STL including leading spaces
--
-- >>> Text.Parsec.parse (parseLoop' <* Text.Parser.Combinators.eof) "loop" "\n  \n  outer loop\nvertex 1 2 3\nvertex 4 5 6\nvertex 7 8 9\nendloop"
-- Right (Loop (LineFeed_ :| [Whitespace_,Whitespace_,LineFeed_,Whitespace_,Whitespace_]) (Whitespace_ :| []) (Vertex3 (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing))) (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit6 :| []) [] Nothing))) (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit7 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit8 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| []) [] Nothing)))) (LineFeed_ :| []))
-- >>> Text.Parsec.parse (parseLoop' <* Text.Parser.Combinators.eof) "loop" "  outer loop\nvertex 1 2 3\n  vertex 4 5 -6  \n   \n  vertex 7 8 9  \n  endloop"
-- Right (Loop (Whitespace_ :| [Whitespace_]) (Whitespace_ :| []) (Vertex3 (Vertex (LineFeed_ :| []) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit1 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit2 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit3 :| []) [] Nothing))) (Vertex (LineFeed_ :| [Whitespace_,Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit4 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit5 :| []) [] Nothing) (Whitespace_ :| []) (Number Minus (DecDigit6 :| []) [] Nothing))) (Vertex (Whitespace_ :| [Whitespace_,LineFeed_,Whitespace_,Whitespace_,Whitespace_,LineFeed_,Whitespace_,Whitespace_]) (ThreeOf (Whitespace_ :| []) (Number Nosign (DecDigit7 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit8 :| []) [] Nothing) (Whitespace_ :| []) (Number Nosign (DecDigit9 :| []) [] Nothing)))) (Whitespace_ :| [Whitespace_,LineFeed_,Whitespace_,Whitespace_]))
parseLoop' ::
  (Monad p, CharParsing p) =>
  p Loop'
parseLoop' =
  parseLoop parseIsoLatin1 parseNumber

-- | Print the "outer loop" up to "endloop" of STL including leading spaces
printLoop ::
  Printer a
  -> Printer n
  -> Printer (Loop a n)
printLoop pa pn =
  divide
    (\(Loop s1 s2 v3 s3) -> (((s1, s2), v3), s3))
    (
      divided
        (
          divided
            (listPrinter pa)
            (str "outer" <> listPrinter pa)
        )
        (str "loop" <> printVertex3 pa pn)
    )
    (listPrinter pa <> str "endloop")

-- | Print the "outer loop" up to "endloop" of STL including leading spaces
--
-- >>> printer printLoop' (Loop (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Vertex3.Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit6 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit7 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit8 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 Data.List.NonEmpty.:| []) [] Nothing)))) (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []))
-- "\n  \n  outer loop\nvertex 1 2 3\nvertex 4 5 6\nvertex 7 8 9\nendloop"
-- >>> printer printLoop' (Loop (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_]) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Vertex3.Vertex3 (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| []) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit1 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit2 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit3 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.LineFeed_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit4 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit5 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Minus (Data.Digit.DecDigit6 Data.List.NonEmpty.:| []) [] Nothing))) (Data.STL.Vertex.Vertex (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]) (Data.STL.ThreeOf.ThreeOf (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit7 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit8 Data.List.NonEmpty.:| []) [] Nothing) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| []) (Data.STL.Number.Number Data.STL.Number.Nosign (Data.Digit.DecDigit9 Data.List.NonEmpty.:| []) [] Nothing)))) (Data.Char.Space.Whitespace_ Data.List.NonEmpty.:| [Data.Char.Space.Whitespace_,Data.Char.Space.LineFeed_,Data.Char.Space.Whitespace_,Data.Char.Space.Whitespace_]))
-- "  outer loop\nvertex 1 2 3\n  vertex 4 5 -6  \n   \n  vertex 7 8 9  \n  endloop"
printLoop' ::
  Printer Loop'
printLoop' =
  printLoop printIsoLatin1 printNumber

-- | Given a parse function, provide the prism to print and parse.
printParseLoop ::
  (CharParsing p, Monad p) =>
  Printer a
  -> Printer n
  -> p a
  -> p n
  -> (p (Loop a n) -> String -> Maybe (Loop a n))
  -> Prism' String (Loop a n)
printParseLoop ra rn pa pn parser =
  prism'
    (printer (printLoop ra rn))
    (parser (parseLoop pa pn))

-- | Given a parse function, provide the prism to print and parse.
printParseLoop' ::
  (CharParsing p, Monad p) =>
  (p Loop' -> String -> Maybe Loop')
  -> Prism' String Loop'
printParseLoop' =
  printParseLoop
    printIsoLatin1
    printNumber
    parseIsoLatin1
    parseNumber

parsecPrintParseLoop' ::
  Prism' String Loop'
parsecPrintParseLoop' =
  printParseLoop' (\p s -> preview _Right (Parsec.parse p "Loop" s))

parsecPrintParseLoop ::
  Printer a
  -> Printer n
  -> Parsec String () a
  -> Parsec String () n
  -> Prism' String (Loop a n)
parsecPrintParseLoop ra rn pa pn =
  printParseLoop ra rn pa pn (\p s -> preview _Right (Parsec.parse p "Loop" s))
